import frozendict
import builtins
import collections
import enum
import functools
import inspect
import itertools
import pathlib
import queue
import re
import sys
import threading


class PointIds(enum.Enum):
    SET = enum.auto()
    GET = enum.auto()
    DEL = enum.auto()
    EXCEPTION = enum.auto()
    LOCK_ACQUIRE = enum.auto()
    LOCK_WAITING = enum.auto()
    LOCK_RELEASE = enum.auto()
    LOCK_LOCKED = enum.auto()
    EVENT_IS_SET = enum.auto()
    EVENT_SET = enum.auto()
    EVENT_CLEAR = enum.auto()
    EVENT_WAIT = enum.auto()
    EVENT_WAITING = enum.auto()
    CONDITION_WAIT = enum.auto()
    CONDITION_WAITING = enum.auto()
    CONDITION_NOTIFY = enum.auto()
    CONDITION_NOTIFY_ALL = enum.auto()


for point_id in PointIds:
    globals()[point_id.name] = point_id


class LockStates(enum.Enum):
    RELEASED = enum.auto()
    ACQUIRED = enum.auto()


RELEASED = LockStates.RELEASED
ACQUIRED = LockStates.ACQUIRED


class _RESET:
    pass


class _STOP:
    pass


def process_code(
    initial_state, entry_functions, reset_function=None, state_function=None
):
    _ = entry_functions.items()
    entry_functions = {
        thread: functions for thread, functions in _ if len(functions) != 0
    }

    if len(entry_functions) == 0:
        return ({}, set())

    # prepare initial state
    initial_state = initial_state.copy()
    for thread in entry_functions:
        initial_state.ats[thread] = ()

    initial_state = initial_state.get_frozen()
    #

    states = {initial_state: (None, ())}

    # prepare
    todo = [(initial_state, [])]

    next_queue = queue.Queue()
    threads = [
        Thread(thread.id, _next_queue=next_queue, _entry_functions=entry_functions)
        for thread in entry_functions
    ]

    single_thread = threads[0] if len(threads) == 1 else None

    if single_thread is None:
        for thread in threads:
            thread._start()
    #

    while todo:
        brute_force = _BruteForce(
            threads.copy(), single_thread, todo, states, initial_state, state_function
        )
        if reset_function is not None:
            try:
                reset_function(brute_force)

            except:
                for thread in threads:
                    thread._queue.put(_STOP)

                raise

        if single_thread is None:
            for thread in threads:
                thread._queue.put(brute_force)

            threads[0]._queue.put(None)  # kick-off

            exception = next_queue.get()
            if exception is not None:
                for thread in threads:
                    thread._queue.put(_RESET)
                    thread._queue.put(_STOP)

                raise exception

            for thread in threads:
                thread._queue.put(_RESET)

        else:
            single_thread._do(brute_force)

            next_queue.get()

    if single_thread is None:
        for thread in threads:
            thread._queue.put(_STOP)

    del states[initial_state]
    return states


class Thread:
    def __init__(self, id, _next_queue=None, _entry_functions=None):
        super().__init__()

        self.id = id
        self._next_queue = _next_queue
        self._entry_functions = _entry_functions

        self._thread = threading.Thread(target=self._target)
        self._queue = queue.Queue()

    def _start(self):
        self._thread.start()

    def _target(self):
        try:
            brute_force = None

            while True:
                object = self._queue.get()

                if object is _RESET:
                    continue

                if object is _STOP:
                    break

                if object is not None:
                    brute_force = object
                    continue

                self._do(brute_force)

        except Exception as exception:
            self._next_queue.put(exception)

    def _do(self, brute_force):
        try:
            this_thread = brute_force._get_thread()

            while True:
                entry_function = brute_force._replay()
                if entry_function is None:
                    _ = self._entry_functions[this_thread]
                    entry_function = brute_force._choose(_)

                entry_function(brute_force)

                brute_force.state.ats[this_thread] = ()
                brute_force._point(True, this_thread)

        except _Next:
            self._next_queue.put(None)

        except _Reset:
            pass

        except Exception as exception:
            traceback_object = sys.exc_info()[2]
            while traceback_object.tb_next is not None:
                traceback_object = traceback_object.tb_next

            frame = traceback_object.tb_frame
            if frame.f_code.co_filename == __file__:
                if isinstance(exception, Deadlock):
                    frame = None  # keep current ats

                elif isinstance(exception, (AttributeError, RuntimeError)):
                    while frame.f_code.co_filename == __file__:
                        frame = frame.f_back

                else:
                    raise exception

            if frame is not None:
                brute_force.state.ats[this_thread] = brute_force._get_at(
                    frame, EXCEPTION
                )

            brute_force.state.exception = (type(exception), exception.args)

            try:
                brute_force._point(False, None)

            except _Next:
                pass

            self._next_queue.put(None)

    def __hash__(self):
        return hash((Thread, self.id))

    def __eq__(self, object):
        return isinstance(object, Thread) and object.id == self.id

    def __repr__(self):
        return f"{type(self).__name__}(id={repr(self.id)})"


class _Next(Exception):
    pass


class _Reset(Exception):
    pass


class _BruteForce:
    def __init__(
        self, threads, single_thread, todo, states, initial_state, state_function
    ):
        super().__init__()

        self.threads = threads
        self.single_thread = single_thread
        self.todo = todo
        self.states = states
        self.initial_state = initial_state
        self.state_function = state_function

        self.state = initial_state.copy(_brute_force=self)

        self._previous_state, self._replay_directions = todo.pop()
        for _, directions in get_history(self._previous_state, states):
            self._replay_directions.extend(reversed(directions))

        self._directions = []

    def _replay(self):
        if not self._replay_directions:
            return

        direction = self._replay_directions.pop()
        self._directions.append(direction)
        return direction

    def _choose(self, directions):
        directions = iter(directions)
        first_direction = next(directions, None)
        if first_direction is None:
            return

        for direction in directions:
            self._directions.append(direction)
            self.todo.append((self._previous_state, self._directions[::-1]))
            self._directions.pop()

        self._directions.append(first_direction)
        return first_direction

    def point(self, id=None, switch_thread=True, _this_thread=None, _back=0):
        if _this_thread is None:
            _this_thread = self._get_thread()

        frame = inspect.currentframe().f_back
        for _ in range(_back):
            frame = frame.f_back

        self.state.ats[_this_thread] = self._get_at(frame, id)
        self._point(switch_thread, _this_thread)

    def _get_at(self, frame, id):
        at = []
        while frame.f_code.co_filename != __file__:
            _ = Point(
                id=id,
                file_name=frame.f_code.co_filename,
                line_number=frame.f_lineno,
                lasti=frame.f_lasti,
            )
            at.append(_)

            id = None
            frame = frame.f_back

        return tuple(at[::-1])

    def _point(self, switch_thread, this_thread):
        if self._replay_directions:
            if switch_thread:
                self._directions.clear()

        else:
            frozen_state = self.state.get_frozen()

            if frozen_state in self.states:
                _ = self._get_history_length(self._previous_state)
                if _ + len(self._directions) < self._get_history_length(frozen_state):
                    _ = tuple(self._directions)
                    self.states[frozen_state] = (self._previous_state, _)

                raise _Next

            self.states[frozen_state] = (self._previous_state, tuple(self._directions))
            if self.state_function is not None:
                self.state_function(frozen_state)

            if switch_thread:
                self._previous_state = frozen_state
                self._directions.clear()

        if switch_thread:
            self._switch_thread(this_thread)

    def _get_thread(self):
        if self.single_thread is not None:
            return self.single_thread

        ident = threading.get_ident()
        for thread in self.threads:
            if thread._thread.ident == ident:
                return thread

    def _get_history_length(self, state):
        return sum(len(directions) for _, directions in get_history(state, self.states))

    def _switch_thread(self, this_thread):
        if self.single_thread is not None:
            if not self.threads:
                raise Deadlock

            return

        thread = self._replay()
        if thread is None:
            thread = self._choose(self.threads)
            if thread is None:
                raise Deadlock

        if thread is this_thread:
            return

        thread._queue.put(None)
        if this_thread._queue.get() is _RESET:
            raise _Reset

    def Lock(self, id):
        lock = _Lock(id, self)
        lock._set(RELEASED)
        return lock

    def RLock(self, id):
        rlock = _RLock(id, self)
        rlock._set(RLockState(thread=None, count=0))
        return rlock

    def Event(self, id):
        event = _Event(id, self)
        event._set(False, frozendict.frozendict())
        return event

    def Condition(self, id, lock=None):
        condition = _Condition(id, lock, self)

        _ = RLockState(thread=None, count=0) if lock is None else None
        condition._set(_, frozendict.frozendict())

        return condition


Point = collections.namedtuple("Point", ["id", "file_name", "line_number", "lasti"])


class Deadlock(Exception):
    pass


class _Proxy:
    def __init__(self, dictionary, point, del_):
        super().__init__()

        setattr = super().__setattr__
        setattr("dictionary", dictionary)
        setattr("point", point)
        setattr("del_", del_)

    def __setattr__(self, name, object):
        getattr = super().__getattribute__

        point = getattr("point")
        if point is not None:
            point(id=SET, _back=1)

        getattr("dictionary")[name] = object

    def __getattribute__(self, name):
        getattr = super().__getattribute__

        point = getattr("point")
        if point is not None:
            point(id=GET, _back=1)

        dictionary = getattr("dictionary")
        try:
            return dictionary[name]

        except KeyError:
            raise AttributeError

    def __delattr__(self, name):
        getattr = super().__getattribute__

        point = getattr("point")
        if point is not None:
            point(id=DEL, _back=1)

        dictionary = getattr("dictionary")
        try:
            del dictionary[name]

        except KeyError:
            raise AttributeError

    def __del__(self):
        del_ = super().__getattribute__("del_")
        if del_ is not None:
            del_()


class _FrozenProxy(_Proxy):
    def __setattr__(self, name, object):
        _ = f"{repr(type(self).__name__)} object doesn't support attribute assignment"
        raise TypeError(_)

    def __delattr__(self, name):
        _ = f"{repr(type(self).__name__)} object doesn't support attribute deletion"
        raise TypeError(_)


class AttributeError(builtins.AttributeError):
    pass


class State:
    __slots__ = (
        "ats",
        "global_objects",
        "local_objects",
        "exception",
        "_brute_force",
        "_g",
    )

    _PROXY_TYPE = _Proxy

    def __init__(
        self,
        ats=None,
        global_objects=None,
        local_objects=None,
        exception=None,
        _brute_force=None,
    ):
        super().__init__()

        self.ats = {} if ats is None else ats
        self.global_objects = {} if global_objects is None else global_objects
        self.local_objects = {} if local_objects is None else local_objects
        self.exception = exception
        self._brute_force = _brute_force

        self._g = None

    def copy(self, _brute_force=None):
        _ = self.local_objects.items()
        return State(
            ats=dict(self.ats),
            global_objects=dict(self.global_objects),
            local_objects={key: dict(dictionary) for key, dictionary in _},
            exception=self.exception,
            _brute_force=_brute_force,
        )

    def get_frozen(self):
        _ = self.local_objects.items()
        _ = ((key, frozendict.frozendict(objects)) for key, objects in _)
        return _FrozenState(
            frozendict.frozendict(self.ats),
            frozendict.frozendict(self.global_objects),
            frozendict.frozendict(_),
            self.exception,
        )

    def g(self):
        if self._g is not None:
            return self._g

        _ = self._brute_force.point
        self._g = type(self)._PROXY_TYPE(self.global_objects, _, None)

        return self._g

    def l(self, key=None):
        if key is not None:
            return type(self)._PROXY_TYPE(self.local_objects[key], None, None)

        back_frame = inspect.currentframe().f_back

        proxy = back_frame.f_locals.get(_Proxy)
        if proxy is not None:
            return proxy

        # get key
        key = []
        frame = back_frame

        while frame.f_code.co_filename != __file__:
            key.append(frame.f_code.co_name)
            frame = frame.f_back

        key.append(self._brute_force._get_thread())
        key = tuple(reversed(key))
        #

        dictionary = {}
        self.local_objects[key] = dictionary

        _ = functools.partial(self.local_objects.pop, key)
        proxy = type(self)._PROXY_TYPE(dictionary, None, _)

        back_frame.f_locals[_Proxy] = proxy  # WARNING key is not a string
        return proxy

    def __hash__(self):
        raise TypeError(f"unhashable type: {repr(type(self).__name__)}")

    def __eq__(self, object):
        return (
            isinstance(object, State)
            and object.ats == self.ats
            and object.global_objects == self.global_objects
            and object.local_objects == self.local_objects
            and object.exception == self.exception
        )

    def __repr__(self):
        return (
            f"{type(self).__name__}(ats={repr(self.ats)},"
            f" global_objects={repr(self.global_objects)},"
            f" local_objects={repr(self.local_objects)},"
            f" exception={repr(self.exception)})"
        )


class _FrozenState(State):
    __slots__ = ()

    def __init__(self, ats, global_objects, local_objects, exception):
        setattr = super().__setattr__
        setattr("ats", ats)
        setattr("global_objects", global_objects)
        setattr("local_objects", local_objects)
        setattr("exception", exception)

    def g(self):
        g = getattr(self, "_g", None)
        if g is not None:
            return g

        g = _FrozenProxy(self.global_objects, None, None)
        super().__setattr__("_g", g)
        return g

    def __hash__(self):
        _ = (
            _FrozenState,
            self.ats,
            self.global_objects,
            self.local_objects,
            self.exception,
        )
        return hash(_)

    def __setattr__(self, name, object):
        _ = f"{repr(type(self).__name__)} object doesn't support attribute assignment"
        raise TypeError(_)

    def __delattr__(self, name):
        _ = f"{repr(type(self).__name__)} object doesn't support attribute deletion"
        raise TypeError(_)


class _LockBase:
    def __init__(self, get, set, brute_force):
        super().__init__()

        self.get = get
        self.set = set
        self.brute_force = brute_force

        self._threads = []

    def acquire(self, blocking, timeout, back):
        if self.locked():
            if not blocking:
                return False

            this_thread = self.brute_force._get_thread()

            if timeout == -1:
                self.brute_force.threads.remove(this_thread)
                self._threads.append(this_thread)

                self.brute_force.point(
                    id=LOCK_WAITING, _this_thread=this_thread, _back=back + 1
                )

                self._threads.remove(this_thread)

            else:
                self.brute_force.point(
                    id=LOCK_WAITING, _this_thread=this_thread, _back=back + 1
                )

                if timeout == 0 or self.locked():
                    return False

                timeout = self.brute_force._replay()
                if timeout is None:
                    timeout = self.brute_force._choose([False, True])

                if timeout:
                    return False

        self.set(ACQUIRED)

        for thread in self._threads:
            self.brute_force.threads.remove(thread)

        return True

    def release(self):
        if not self.locked():
            raise RuntimeError

        self.set(RELEASED)

        self.brute_force.threads.extend(self._threads)

    def locked(self):
        return self.get() is ACQUIRED


class _Lock:
    def __init__(self, id, brute_force):
        super().__init__()

        self.id = id
        self.brute_force = brute_force

        self._lock_base = _LockBase(self._get, self._set, brute_force)
        self._lock = Lock(id=id)

    def _get(self):
        return self.brute_force.state.global_objects[self._lock]

    def _set(self, lock_state):
        self.brute_force.state.global_objects[self._lock] = lock_state

    def acquire(self, blocking=True, timeout=-1, _back=0):
        self.brute_force.point(id=LOCK_ACQUIRE, _back=_back + 1)
        return self._lock_base.acquire(blocking, timeout, _back + 1)

    def release(self, _back=0):
        self.brute_force.point(id=LOCK_RELEASE, _back=_back + 1)
        self._lock_base.release()

    def locked(self):
        self.brute_force.point(id=LOCK_LOCKED, _back=1)
        return self._get() is ACQUIRED

    def __enter__(self):
        self.acquire(_back=1)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if isinstance(exc_value, (_Next, _Reset, Deadlock)):
            return

        self.release(_back=1)

    def __hash__(self):
        raise TypeError(f"unhashable type: {repr(type(self).__name__)}")


Lock = collections.namedtuple("Lock", ["id"])


class _RLockBase:
    def __init__(self, get, set, brute_force):
        super().__init__()

        self.get = get
        self.set = set
        self.brute_force = brute_force

        self._lock_base = _LockBase(self._get, self._set, brute_force)

    def _get(self):
        return ACQUIRED if self.locked() else RELEASED

    def _set(self, lock_state):
        pass

    def acquire(self, blocking, timeout, back, count=1):
        this_thread = self.brute_force._get_thread()
        rlock_state = self.get()

        if rlock_state.thread is None:
            self._lock_base.acquire(blocking, timeout, back + 1)
            self.set(RLockState(thread=this_thread, count=count))
            return True

        if rlock_state.thread is this_thread:
            self.set(RLockState(thread=this_thread, count=rlock_state.count + count))
            return True

        if self._lock_base.acquire(blocking, timeout, back + 1):
            self.set(RLockState(thread=this_thread, count=count))
            return True

        return False

    def release(self, all=False):
        this_thread = self.brute_force._get_thread()
        rlock_state = self.get()

        if rlock_state.thread is not this_thread or rlock_state.count == 0:
            raise RuntimeError

        if rlock_state.count == 1 or all:
            self._lock_base.release()
            self.set(RLockState(thread=None, count=0))

        else:
            self.set(RLockState(thread=this_thread, count=rlock_state.count - 1))

        return rlock_state.count if all else 1

    def locked(self):
        return self.get().thread is not None


class _RLock:
    def __init__(self, id, brute_force):
        super().__init__()

        self.id = id
        self.brute_force = brute_force

        self._rlock_base = _RLockBase(self._get, self._set, brute_force)
        self._rlock = RLock(id=id)

    def _get(self):
        return self.brute_force.state.global_objects[self._rlock]

    def _set(self, lock_state):
        self.brute_force.state.global_objects[self._rlock] = lock_state

    def acquire(self, blocking=True, timeout=-1, _back=0):
        self.brute_force.point(id=LOCK_ACQUIRE, _back=_back + 1)
        return self._rlock_base.acquire(blocking, timeout, _back + 1)

    def release(self, _back=0):
        self.brute_force.point(id=LOCK_RELEASE, _back=_back + 1)
        self._rlock_base.release()

    def __enter__(self):
        self.acquire(_back=1)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if isinstance(exc_value, (_Next, _Reset, Deadlock)):
            return

        self.release(_back=1)

    def __hash__(self):
        raise TypeError(f"unhashable type: {repr(type(self).__name__)}")


RLock = collections.namedtuple("RLock", ["id"])
RLockState = collections.namedtuple("RLockState", ["thread", "count"])


class RuntimeError(builtins.RuntimeError):
    pass


class _Event:
    def __init__(self, id, brute_force):
        super().__init__()

        self.id = id
        self.brute_force = brute_force

        self._event = Event(id=id)

    def _set(self, set, threads):
        self.brute_force.state.global_objects[self._event] = EventState(
            set=set, threads=threads
        )

    def _get(self):
        return self.brute_force.state.global_objects[self._event]

    def is_set(self):
        self.brute_force.point(id=EVENT_IS_SET, _back=1)
        return self._get().set

    def set(self):
        self.brute_force.point(id=EVENT_SET, _back=1)

        event_state = self._get()

        self._set(True, frozendict.frozendict.fromkeys(event_state.threads, True))

        threads = self.brute_force.threads

        _ = event_state.threads.items()
        _ = (thread for thread, awake in _ if not awake and thread not in threads)
        threads.extend(_)

    def clear(self):
        self.brute_force.point(id=EVENT_CLEAR, _back=1)

        self._set(False, self._get().threads)

    def wait(self, timeout=None):
        this_thread = self.brute_force._get_thread()

        self.brute_force.point(id=EVENT_WAIT, _this_thread=this_thread, _back=1)

        event_state = self._get()

        if event_state.set:
            return True

        if timeout is None:
            self.brute_force.threads.remove(this_thread)
            self._set(False, _frozendict_set(event_state.threads, this_thread, False))

            self.brute_force.point(id=EVENT_WAITING, _this_thread=this_thread, _back=1)

            event_state = self._get()
            _ = _frozendict_delete(event_state.threads, this_thread)
            self._set(event_state.set, _)

            return True

        self._set(False, _frozendict_set(event_state.threads, this_thread, False))

        self.brute_force.point(id=EVENT_WAITING, _this_thread=this_thread, _back=1)

        event_state = self._get()

        self._set(event_state.set, _frozendict_delete(event_state.threads, this_thread))

        if timeout == 0 or not event_state.threads[this_thread]:
            return False

        timeout = self.brute_force._replay()
        if timeout is None:
            timeout = self.brute_force._choose([False, True])

        return not timeout

    def __hash__(self):
        raise TypeError(f"unhashable type: {repr(type(self).__name__)}")


Event = collections.namedtuple("Event", ["id"])
EventState = collections.namedtuple("EventState", ["set", "threads"])


class _Condition:
    def __init__(self, id, lock, brute_force):
        super().__init__()

        self.id = id
        self.lock = lock
        self.brute_force = brute_force

        if lock is None:
            self._lock_base = _RLockBase(self._lock_get, self._lock_set, brute_force)

        elif isinstance(lock, _Lock):
            self._lock_base = lock._lock_base

        elif isinstance(lock, _RLock):
            self._lock_base = lock._rlock_base

        else:
            raise TypeError(type(lock))

        self._condition = Condition(id=id)

    def _get(self):
        return self.brute_force.state.global_objects[self._condition]

    def _set(self, lock_state, threads):
        self.brute_force.state.global_objects[self._condition] = ConditionState(
            lock=lock_state, threads=threads
        )

    def _lock_get(self):
        return self._get().lock

    def _lock_set(self, lock_state):
        condition_state = self._get()
        self._set(lock_state, condition_state.threads)

    def acquire(self, blocking=True, timeout=-1, _back=0):
        self.brute_force.point(id=LOCK_ACQUIRE, _back=_back + 1)
        return self._lock_base.acquire(blocking, timeout, _back + 1)

    def release(self, _back=0):
        self.brute_force.point(id=LOCK_RELEASE, _back=_back + 1)
        self._lock_base.release()

    def wait(self, timeout=None, _back=0):
        is_rlock = isinstance(self._lock_base, _RLockBase)
        this_thread = self.brute_force._get_thread()

        self.brute_force.point(
            id=CONDITION_WAIT, _this_thread=this_thread, _back=_back + 1
        )

        if is_rlock:
            count = self._lock_base.release(all=True)
        else:
            self._lock_base.release()

        condition_state = self._get()

        if timeout is None:
            self.brute_force.threads.remove(this_thread)

            _ = _frozendict_set(condition_state.threads, this_thread, False)
            self._set(condition_state.lock, _)

            self.brute_force.point(
                id=CONDITION_WAITING, _this_thread=this_thread, _back=_back + 1
            )

            condition_state = self._get()
            _ = _frozendict_delete(condition_state.threads, this_thread)
            self._set(condition_state.lock, _)

            if is_rlock:
                self._lock_base.acquire(True, -1, 1, count=count)
            else:
                self._lock_base.acquire(True, -1, 1)

            return True

        _ = _frozendict_set(condition_state.threads, this_thread, False)
        self._set(condition_state.lock, _)

        self.brute_force.point(
            id=CONDITION_WAITING, _this_thread=this_thread, _back=_back + 1
        )

        condition_state = self._get()

        _ = _frozendict_delete(condition_state.threads, this_thread)
        self._set(condition_state.lock, _)

        if is_rlock:
            self._lock_base.acquire(True, -1, 1, count=count)
        else:
            self._lock_base.acquire(True, -1, 1)

        if timeout == 0 or not condition_state.threads[this_thread]:
            return False

        timeout = self.brute_force._replay()
        if timeout is None:
            timeout = self.brute_force._choose([False, True])

        return not timeout

    def wait_for(self, predicate, timeout=None):
        while not predicate():
            if not self.wait(timeout=timeout, _back=1):
                return False

        return True

    def notify(self, n=1):
        self.brute_force.point(id=CONDITION_NOTIFY, _back=1)

        if not self._lock_base.locked():
            raise RuntimeError

        condition_state = self._get()

        _ = condition_state.threads.items()
        threads = [thread for thread, awake in _ if not awake]

        if len(threads) == 0:
            return

        if len(threads) <= n:
            _ = frozendict.frozendict.fromkeys(condition_state.threads, True)
            self._set(condition_state.lock, _)

            self.brute_force.threads.extend(threads)

        else:
            wake_threads = self.brute_force._replay()
            if wake_threads is None:
                _ = itertools.combinations(threads, n)
                wake_threads = self.brute_force._choose(_)

            _ = condition_state.threads | dict.fromkeys(wake_threads, True)
            self._set(condition_state.lock, frozendict.frozendict(_))

            self.brute_force.threads.extend(wake_threads)

    def notify_all(self):
        self.brute_force.point(id=CONDITION_NOTIFY_ALL, _back=1)

        if not self._lock_base.locked():
            raise RuntimeError

        condition_state = self._get()

        _ = frozendict.frozendict.fromkeys(condition_state.threads, True)
        self._set(condition_state.lock, _)

        _ = (thread for thread, awake in condition_state.threads.items() if not awake)
        self.brute_force.threads.extend(_)

    def __enter__(self):
        self.acquire(_back=1)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if isinstance(exc_value, (_Next, _Reset, Deadlock)):
            return

        self.release(_back=1)


Condition = collections.namedtuple("Condition", ["id"])
ConditionState = collections.namedtuple("ConditionState", ["lock", "threads"])


def get_history(state, states):
    while True:
        tuple = states.get(state)
        if tuple is None:
            break

        yield tuple
        state = tuple[0]


def format_history(state, states, code_string=None, rich=False):
    import pandas

    rows = []
    previous_row = {}

    _ = itertools.chain(reversed(list(get_history(state, states))), [(state, None)])
    for state, _ in _:
        row = {}

        row.update((("at", thread), at) for thread, at in state.ats.items())

        _ = ((("global", key), object) for key, object in state.global_objects.items())
        row.update(_)

        for local_key, local_objects in state.local_objects.items():
            local_name = "/".join(map(str, local_key))

            _ = (((local_name, key), object) for key, object in local_objects.items())
            row.update(_)

        if state.exception is not None:
            exception_type, exception_args = state.exception
            row[("exception", "type")] = exception_type
            row[("exception", "args")] = exception_args

        _ = {
            key: pandas.NA if previous_row.get(key, _None) == object else object
            for key, object in row.items()
        }
        rows.append(_)

        previous_row.update(row)

    empty_row = {column: pandas.NA for row in rows for column in row}
    data_frame = pandas.DataFrame(empty_row | row for row in rows)

    formatter = _Formatter(code_string)

    for column, series in data_frame.items():
        if column[0] == "at":
            series = series.apply(formatter).apply(str)
            series = series.str.ljust(series.str.len().max())

        elif column == ("exception", "type"):

            def _(type):
                if pandas.isna(type):
                    return ""
                if type.__module__ == "builtins":
                    return type.__name__
                return f"{type.__module__}.{type.__name__}"

            series = series.apply(_)

        else:
            series = series.apply(formatter)

        data_frame[column] = series

    _ = (
        tuple(
            object if isinstance(object, str) else formatter(object)
            for object in tuple_
        )
        for tuple_ in data_frame.columns
    )
    data_frame.columns = pandas.MultiIndex.from_tuples(_)

    if rich:
        import rich.table as r_table
        import rich.text as r_text

        table = r_table.Table()

        table.add_column("")

        previous_group = None
        for group_name, name in data_frame.columns:
            _ = f"\n{name}" if group_name == previous_group else f"{group_name}\n{name}"
            table.add_column(_)
            previous_group = group_name

        strings = {}
        for index, row in data_frame.iterrows():
            renderables = [str(index)]
            for column, string in row.items():
                if string.strip() == "":
                    renderable = strings.get(column, "")

                else:
                    renderable = r_text.Text.assemble((string, "#ff8000"))
                    strings[column] = string

                renderables.append(renderable)

            table.add_row(*renderables)

        return table

    return data_frame.to_string()


class _Formatter:
    def __init__(self, code_string):
        super().__init__()

        self.code_string = code_string

        self._line_strings = {}

        self._add_code_string("<string>", code_string)

    def __call__(self, object, sub=False):
        import pandas

        if object is pandas.NA:
            return object if sub else ""

        if isinstance(object, frozendict.frozendict):
            object = dict(object)
            return object if sub else repr(object)

        if isinstance(object, enum.Enum):
            return _String(object.name)

        if isinstance(object, Point):
            parts = []

            if object.file_name != "<string>":
                parts.append(object.file_name)

            line_strings = self._get_lines(object.file_name)

            string = str(object.line_number)
            _ = line_strings is None
            parts.append(string if _ else string.rjust(len(str(len(line_strings)))))

            if line_strings is not None:
                _ = self._format_line(line_strings[object.line_number - 1])
                parts.append(f"|{_}|")

            if object.id is not None:
                parts.append(self(object.id))

            return _String(" ".join(parts))

        if isinstance(object, (RLockState, EventState, ConditionState)):
            object = type(object)(*(self(object, sub=True) for object in object))
            return object if sub else repr(object)

        if type(object) is tuple:
            object = tuple(self(object, sub=True) for object in object)
            return object if sub else repr(object)

        return object if sub else repr(object)

    def _get_lines(self, file_name):
        line_strings = self._line_strings.get(file_name, _None)
        if line_strings is not _None:
            return line_strings

        path = pathlib.Path(file_name)
        if not path.exists():
            return self._add_code_string(file_name, None)

        with open(path, "r") as file:
            code_string = file.read()

        return self._add_code_string(file_name, code_string)

    def _add_code_string(self, file_name, code_string):
        if code_string is None:
            self._line_strings[file_name] = None
            return

        line_strings = code_string.splitlines()
        self._line_strings[file_name] = line_strings
        return line_strings

    def _format_line(self, line_string):
        n = 23

        _ = lambda match: " " * (match.end() // 4)
        line_string = re.sub(r"^([ ]{4})+", _, line_string)

        if len(line_string) < n:
            return line_string.ljust(n)

        return f"{line_string[: n - 2]}.."


class _None:
    pass


class _String(str):
    def __repr__(self):
        return str(self)


def _frozendict_set(frozen_dict, key, object):
    """
    - frozendict.set uses deepcopy
    """
    dictionary = dict(frozen_dict)
    dictionary[key] = object
    return frozendict.frozendict(dictionary)


def _frozendict_delete(frozen_dict, key):
    """
    - frozendict.delete uses deepcopy
    """
    dictionary = dict(frozen_dict)
    del dictionary[key]
    return frozendict.frozendict(dictionary)
