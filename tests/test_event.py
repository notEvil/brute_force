import brute_force
import tests.common as t_common
import frozendict
import itertools


for name, object in vars(t_common).items():
    if name.startswith("__"):
        continue

    globals()[name] = object


def ES(set, ts=None):
    _ = {} if ts is None else ts
    return brute_force.EventState(set=set, threads=frozendict.frozendict(_))


def test_init():
    code_string = """
event = None

def reset(brute_force):
    global event

    event = brute_force.Event("e1")

def f(brute_force):
    pass
""".strip()

    expected_states = {S(ats={T1: ()}, g={brute_force.Event("e1"): ES(False)})}

    _test(code_string, {T1: {"f"}}, S(), expected_states, reset_name="reset")


def test_is_set():
    e1 = brute_force.Event("e1")

    for set in [False, True]:
        code_string = f"""
event = None

def reset(brute_force):
    global event

    event = brute_force.Event("e1")

def f(brute_force):
    if {set}:
        event.set()  # 10

    assert event.is_set() is {set}  # 12
    raise Exception  # 13
""".strip()

        g0 = {e1: ES(False)}
        g1 = {e1: ES(set)}

        if set:
            expected_states = {
                S(ats={T1: At(EVENT_SET, 10)}, g=g0),
                S(ats={T1: At(EVENT_IS_SET, 12)}, g=g1),
                S(ats={T1: At(EXCEPTION, 13)}, g=g1, e=(Exception, ())),
            }

        else:
            expected_states = {
                S(ats={T1: At(EVENT_IS_SET, 12)}, g=g1),
                S(ats={T1: At(EXCEPTION, 13)}, g=g1, e=(Exception, ())),
            }

        _test(code_string, {T1: {"f"}}, S(g=g0), expected_states, reset_name="reset")


def test_set():
    e1 = brute_force.Event("e1")

    for set, thread in itertools.product([False, True], [None, False, True]):
        if set and thread is False:
            continue

        code_string = f"""
import brute_force as bf_module
import frozendict

event = None

def reset(brute_force):
    global event

    event = brute_force.Event("e1")

def f2(brute_force):
    event.wait()  # 12
    raise Exception

def f1(brute_force):
    event.set()  # 16
    if not {set}:
        event.clear()  # 18
    if {thread} is not None:
        brute_force.point()  # 20
        if brute_force.state.global_objects[bf_module.Event("e1")] != bf_module.EventState(set={set}, threads=frozendict.frozendict({{bf_module.Thread("t2"): {thread}}})):
            raise Exception  # 22

    event.set()  # 24
    raise Exception  # 25
""".strip()

        g0 = {e1: ES(False)}
        g1 = {e1: ES(set, ts={} if thread is None else {T2: thread})}

        if thread is None:
            if set:
                expected_states = {
                    S(ats={T1: At(EVENT_SET, 16)}, g=g0),
                    S(ats={T1: At(EVENT_SET, 24)}, g=g1),
                    S(ats={T1: At(EXCEPTION, 25)}, g=g1, e=(Exception, ())),
                }

            else:
                expected_states = {
                    S(ats={T1: At(EVENT_SET, 16)}, g=g0),
                    S(ats={T1: At(EVENT_CLEAR, 18)}, g={e1: ES(True)}),
                    S(ats={T1: At(EVENT_SET, 24)}, g=g1),
                    S(ats={T1: At(EXCEPTION, 25)}, g={e1: ES(True)}, e=(Exception, ())),
                }

        else:
            p2 = At(EVENT_WAITING, 12)

            _ = ES(True, ts={T2: True})
            _ = S(ats={T1: At(EXCEPTION, 25), T2: p2}, g={e1: _}, e=(Exception, ()))
            expected_states = {S(ats={T1: At(EVENT_SET, 24), T2: p2}, g=g1), _}

        _ = {T1: {"f1"}} if thread is None else {T1: {"f1"}, T2: {"f2"}}
        states = _test(code_string, _, S(g=g0), None, reset_name="reset")

        if thread is None:
            _match(states, expected_states, states, code_string)

        else:
            _ = (
                lambda state: len(state.ats[T1]) == 1
                and state.ats[T1][0].line_number not in [16, 18, 20, 22]
                and len(state.ats[T2]) == 1
                and state.ats[T2][0].line_number == 12
                and state.ats[T2][0].id == EVENT_WAITING
            )
            _match(filter(_, states), expected_states, states, code_string)


def test_clear():
    e1 = brute_force.Event("e1")

    for set, thread in itertools.product([False, True], [None, False, True]):
        if set and thread is False:
            continue

        code_string = f"""
import brute_force as bf_module
import frozendict

event = None

def reset(brute_force):
    global event

    event = brute_force.Event("e1")

def f2(brute_force):
    event.wait()  # 12
    raise Exception

def f1(brute_force):
    event.set()  # 16
    if not {set}:
        event.clear()  # 18
    if {thread} is not None:
        brute_force.point()  # 20
        if brute_force.state.global_objects[bf_module.Event("e1")] != bf_module.EventState(set={set}, threads=frozendict.frozendict({{bf_module.Thread("t2"): {thread}}})):
            raise Exception  # 22

    event.clear()  # 24
    raise Exception  # 25
""".strip()

        g0 = {e1: ES(False)}
        g1 = {e1: ES(set, ts={} if thread is None else {T2: thread})}

        if thread is None:
            g2 = g0

            if set:
                expected_states = {
                    S(ats={T1: At(EVENT_SET, 16)}, g=g0),
                    S(ats={T1: At(EVENT_CLEAR, 24)}, g=g1),
                    S(ats={T1: At(EXCEPTION, 25)}, g=g2, e=(Exception, ())),
                }

            else:
                expected_states = {
                    S(ats={T1: At(EVENT_SET, 16)}, g=g0),
                    S(ats={T1: At(EVENT_CLEAR, 18)}, g={e1: ES(True)}),
                    S(ats={T1: At(EVENT_CLEAR, 24)}, g=g1),
                    S(ats={T1: At(EXCEPTION, 25)}, g=g2, e=(Exception, ())),
                }

        else:
            p2 = At(EVENT_WAITING, 12)

            _ = ES(False, ts={T2: thread})
            _ = S(ats={T1: At(EXCEPTION, 25), T2: p2}, g={e1: _}, e=(Exception, ()))
            expected_states = {S(ats={T1: At(EVENT_CLEAR, 24), T2: p2}, g=g1), _}

        _ = {T1: {"f1"}} if thread is None else {T1: {"f1"}, T2: {"f2"}}
        states = _test(code_string, _, S(g=g0), None, reset_name="reset")

        if thread is None:
            _match(states, expected_states, states, code_string)

        else:
            _ = (
                lambda state: len(state.ats[T1]) == 1
                and state.ats[T1][0].line_number not in [16, 18, 20, 22]
                and len(state.ats[T2]) == 1
                and state.ats[T2][0].line_number == 12
                and state.ats[T2][0].id == EVENT_WAITING
            )
            _match(filter(_, states), expected_states, states, code_string)


def test_wait():
    e1 = brute_force.Event("e1")

    _ = itertools.product([False, True], [None, False, True], [None, 0, 1])
    for set, thread, timeout in _:
        if set and thread is False:
            continue

        code_string = f"""
import brute_force as bf_module
import frozendict

event = None

def reset(brute_force):
    global event

    event = brute_force.Event("e1")

def f2(brute_force):
    event.wait()  # 12
    raise Exception

def f1(brute_force):
    event.set()  # 16
    if not {set}:
        event.clear()  # 18
    if {thread} is not None:
        brute_force.point()  # 20
        if brute_force.state.global_objects[bf_module.Event("e1")] != bf_module.EventState(set={set}, threads=frozendict.frozendict({{bf_module.Thread("t2"): {thread}}})):
            raise Exception  # 22

    if event.wait(timeout={timeout}):  # 24
        brute_force.point("p1")
    else:
        brute_force.point("p2")
    raise Exception  # 28
""".strip()

        g0 = {e1: ES(False)}
        g1 = {e1: ES(set, ts={} if thread is None else {T2: thread})}

        if thread is None:
            if set:
                expected_states = {
                    S(ats={T1: At(EVENT_SET, 16)}, g=g0),
                    S(ats={T1: At(EVENT_WAIT, 24)}, g=g1),
                    S(ats={T1: At("p1", A)}, g=g1),
                    S(ats={T1: At(EXCEPTION, 28)}, g=g1, e=(Exception, ())),
                }

            else:
                g2 = {e1: ES(False, ts={T1: False})}

                if timeout is None:
                    _ = At(EVENT_WAITING, 24)
                    expected_states = {
                        S(ats={T1: At(EVENT_SET, 16)}, g=g0),
                        S(ats={T1: At(EVENT_CLEAR, 18)}, g={e1: ES(True)}),
                        S(ats={T1: At(EVENT_WAIT, 24)}, g=g1),
                        S(ats={T1: At(EVENT_WAITING, 24)}, g=g2),
                        S(ats={T1: _}, g=g2, e=(brute_force.Deadlock, ())),
                    }

                elif timeout == 0:
                    _ = ES(False, {T1: False})
                    expected_states = {
                        S(ats={T1: At(EVENT_SET, 16)}, g=g0),
                        S(ats={T1: At(EVENT_CLEAR, 18)}, g={e1: ES(True)}),
                        S(ats={T1: At(EVENT_WAIT, 24)}, g=g1),
                        S(ats={T1: At(EVENT_WAITING, 24)}, g={e1: _}),
                        S(ats={T1: At("p2", A)}, g=g1),
                        S(ats={T1: At(EXCEPTION, 28)}, g=g1, e=(Exception, ())),
                    }

                else:
                    expected_states = {
                        S(ats={T1: At(EVENT_SET, 16)}, g=g0),
                        S(ats={T1: At(EVENT_CLEAR, 18)}, g={e1: ES(True)}),
                        S(ats={T1: At(EVENT_WAIT, 24)}, g=g1),
                        S(ats={T1: At(EVENT_WAITING, 24)}, g=g2),
                        S(ats={T1: At("p2", A)}, g=g1),
                        S(ats={T1: At(EXCEPTION, 28)}, g=g1, e=(Exception, ())),
                    }

        else:
            p2 = At(EVENT_WAITING, 12)

            if set:
                expected_states = {
                    S(ats={T1: At(EVENT_WAIT, 24), T2: p2}, g=g1),
                    S(ats={T1: At("p1", A), T2: p2}, g=g1),
                    S(ats={T1: At(EXCEPTION, 28), T2: p2}, g=g1, e=(Exception, ())),
                }

            else:
                g2 = {e1: ES(False, ts={T1: False, T2: thread})}

                if timeout is None:
                    expected_states = {
                        S(ats={T1: At(EVENT_WAIT, 24), T2: p2}, g=g1),
                        S(ats={T1: At(EVENT_WAITING, 24), T2: p2}, g=g2),
                    }

                    if not thread:
                        _ = At(EVENT_WAITING, 24)
                        _ = {S(ats={T1: _, T2: p2}, g=g2, e=(brute_force.Deadlock, ()))}
                        expected_states.update(_)

                elif timeout == 0:
                    expected_states = {
                        S(ats={T1: At(EVENT_WAIT, 24), T2: p2}, g=g1),
                        S(ats={T1: At(EVENT_WAITING, 24), T2: p2}, g=g2),
                        S(ats={T1: At("p2", A), T2: p2}, g=g1),
                        S(ats={T1: At(EXCEPTION, 28), T2: p2}, g=g1, e=(Exception, ())),
                    }

                else:
                    expected_states = {
                        S(ats={T1: At(EVENT_WAIT, 24), T2: p2}, g=g1),
                        S(ats={T1: At(EVENT_WAITING, 24), T2: p2}, g=g2),
                        S(ats={T1: At("p2", A), T2: p2}, g=g1),
                        S(ats={T1: At(EXCEPTION, 28), T2: p2}, g=g1, e=(Exception, ())),
                    }

        _ = {T1: {"f1"}} if thread is None else {T1: {"f1"}, T2: {"f2"}}
        states = _test(code_string, _, S(g=g0), None, reset_name="reset")

        if thread is None:
            _match(states, expected_states, states, code_string)

        else:
            _ = (
                lambda state: len(state.ats[T1]) == 1
                and state.ats[T1][0].line_number not in [16, 18, 20, 22]
                and len(state.ats[T2]) == 1
                and state.ats[T2][0].line_number == 12
                and state.ats[T2][0].id == EVENT_WAITING
            )
            _match(filter(_, states), expected_states, states, code_string)
