import brute_force
import tests.common as t_common
import itertools


for name, object in vars(t_common).items():
    if name.startswith("__"):
        continue

    globals()[name] = object


def LS(thread, count):
    return brute_force.RLockState(thread=thread, count=count)


def test_init():
    code_string = """
r_lock = None

def reset(brute_force):
    global r_lock

    r_lock = brute_force.RLock("l1")

def f(brute_force):
    pass
""".strip()

    expected_states = {S(ats={T1: ()}, g={brute_force.RLock("l1"): LS(None, 0)})}

    _test(code_string, {T1: {"f"}}, S(), expected_states, reset_name="reset")


def test_acquire():
    l1 = brute_force.RLock("l1")

    _ = list(itertools.product([False, True], [{}, {T2: False}, {T2: True}]))
    _ = itertools.product([None, T1, T2], [False, True], [-1, 0, 1], [(None, None)] + _)
    for thread, blocking, timeout, (condition_lock, condition_threads) in _:
        condition_string = (
            "from brute_force import Condition as C, ConditionState as CS, RLockState"
            " as LS, Thread; import frozendict as f;"
            ' brute_force.state.global_objects[C(id="c1")] ='
            f" CS(lock={'LS(thread=None, count=0)' if condition_lock else 'None'},"
            f" threads=f.frozendict({repr(condition_threads)}))"
        )
        lock_string = {
            None: 'brute_force.RLock("l1")',
            False: (
                'brute_force.Condition("c1", lock=brute_force.RLock("l1"));'
                f" {condition_string}"
            ),
            True: f'brute_force.Condition("c1"); {condition_string}',
        }[condition_lock]

        def g(global_objects, thread, count):
            if condition_lock is None:
                global_objects[l1] = LS(thread, count)

            elif condition_lock:
                c1 = brute_force.Condition("c1")
                global_objects[c1] = brute_force.ConditionState(
                    lock=LS(thread, count),
                    threads=frozendict.frozendict(condition_threads),
                )

            else:
                c1 = brute_force.Condition("c1")
                _ = brute_force.ConditionState(
                    lock=None, threads=frozendict.frozendict(condition_threads)
                )
                global_objects.update({c1: _, l1: LS(thread, count)})

            return global_objects

        code_string = f"""
r_lock = None

def reset(brute_force):
    global r_lock

    r_lock = {lock_string}

def f2(brute_force):
    g = brute_force.state.g()

    r_lock.acquire()  # 11
    while True:
        g.i = True  # 13

def f1(brute_force):
    g = brute_force.state.g()

    if {thread is T1}:
        r_lock.acquire()  # 19
    while {thread is T2}:
        if g.i:  # 21
            break

    if r_lock.acquire(blocking={blocking}, timeout={timeout}):  # 24
        brute_force.point("p1")
    else:
        brute_force.point("p2")
    raise Exception  # 28
""".strip()

        g0 = g({"i": False}, None, 0)
        c1 = 0 if thread is None else 1

        if thread is None:
            g1 = g0
            g2 = g({"i": False}, T1, 1)

            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 24)}, g=g1),
                S(ats={T1: At("p1", A)}, g=g2),
                S(ats={T1: At(EXCEPTION, 28)}, g=g2, e=(Exception, ())),
            }

        elif thread is T1:
            g1 = g({"i": False}, thread, c1)
            g2 = g({"i": False}, T1, 2)

            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 19)}, g=g0),
                S(ats={T1: At(LOCK_ACQUIRE, 24)}, g=g1),
                S(ats={T1: At("p1", A)}, g=g2),
                S(ats={T1: At(EXCEPTION, 28)}, g=g2, e=(Exception, ())),
            }

        elif thread is T2:
            p2 = At(SET, 13)
            g1 = g({"i": True}, thread, c1)

            if blocking and timeout == -1:
                expected_states = {
                    S(ats={T1: At(GET, 21), T2: ()}, g=g0),
                    S(ats={T1: At(GET, 21), T2: At(LOCK_ACQUIRE, 11)}, g=g0),
                    S(ats={T1: At(GET, 21), T2: p2}, g=g({"i": False}, thread, c1)),
                    S(ats={T1: At(GET, 21), T2: p2}, g=g1),
                    S(ats={T1: At(LOCK_ACQUIRE, 24), T2: p2}, g=g1),
                    S(ats={T1: At(LOCK_WAITING, 24), T2: p2}, g=g1),
                }

            elif not blocking:
                expected_states = {
                    S(ats={T1: At(GET, 21), T2: ()}, g=g0),
                    S(ats={T1: At(GET, 21), T2: At(LOCK_ACQUIRE, 11)}, g=g0),
                    S(ats={T1: At(GET, 21), T2: p2}, g=g({"i": False}, thread, c1)),
                    S(ats={T1: At(GET, 21), T2: p2}, g=g1),
                    S(ats={T1: At(LOCK_ACQUIRE, 24), T2: p2}, g=g1),
                    S(ats={T1: At("p2", A), T2: p2}, g=g1),
                    S(ats={T1: At(EXCEPTION, 28), T2: p2}, g=g1, e=(Exception, ())),
                }

            else:
                expected_states = {
                    S(ats={T1: At(GET, 21), T2: ()}, g=g0),
                    S(ats={T1: At(GET, 21), T2: At(LOCK_ACQUIRE, 11)}, g=g0),
                    S(ats={T1: At(GET, 21), T2: p2}, g=g({"i": False}, thread, c1)),
                    S(ats={T1: At(GET, 21), T2: p2}, g=g1),
                    S(ats={T1: At(LOCK_ACQUIRE, 24), T2: p2}, g=g1),
                    S(ats={T1: At(LOCK_WAITING, 24), T2: p2}, g=g1),
                    S(ats={T1: At("p2", A), T2: p2}, g=g1),
                    S(ats={T1: At(EXCEPTION, 28), T2: p2}, g=g1, e=(Exception, ())),
                }

        else:
            raise Exception

        _ = {T1: {"f1"}, T2: {"f2"}} if thread is T2 else {T1: {"f1"}}
        _test(code_string, _, S(g=g0), expected_states, reset_name="reset")


def test_release():
    l1 = brute_force.RLock("l1")

    _ = list(itertools.product([False, True], [{}, {T2: False}, {T2: True}]))
    _ = itertools.product([None, T1, T2], [(None, None)] + _)
    for thread, (condition_lock, condition_threads) in _:
        condition_string = (
            "from brute_force import Condition as C, ConditionState as CS, RLockState"
            " as LS, Thread; import frozendict as f;"
            ' brute_force.state.global_objects[C(id="c1")] ='
            f" CS(lock={'LS(thread=None, count=0)' if condition_lock else 'None'},"
            f" threads=f.frozendict({repr(condition_threads)}))"
        )
        lock_string = {
            None: 'brute_force.RLock("l1")',
            False: (
                'brute_force.Condition("c1", lock=brute_force.RLock("l1"));'
                f" {condition_string}"
            ),
            True: f'brute_force.Condition("c1"); {condition_string}',
        }[condition_lock]

        def g(global_objects, thread, count):
            if condition_lock is None:
                global_objects[l1] = LS(thread, count)

            elif condition_lock:
                c1 = brute_force.Condition("c1")
                global_objects[c1] = brute_force.ConditionState(
                    lock=LS(thread, count),
                    threads=frozendict.frozendict(condition_threads),
                )

            else:
                c1 = brute_force.Condition("c1")
                _ = brute_force.ConditionState(
                    lock=None, threads=frozendict.frozendict(condition_threads)
                )
                global_objects.update({c1: _, l1: LS(thread, count)})

            return global_objects

        code_string = f"""
r_lock = None

def reset(brute_force):
    global r_lock

    r_lock = {lock_string}

def f2(brute_force):
    g = brute_force.state.g()

    r_lock.acquire()  # 11
    while True:
        g.i = True  # 13

def f1(brute_force):
    g = brute_force.state.g()

    if {thread is T1}:
        r_lock.acquire()  # 19
    while {thread is T2}:
        if g.i:  # 21
            break

    r_lock.release()  # 24
    raise Exception  # 25
""".strip()

        g0 = g({"i": False}, None, 0)
        c1 = 0 if thread is None else 1

        if thread is None:
            g1 = g0

            expected_states = {
                S(ats={T1: At(LOCK_RELEASE, 24)}, g=g1),
                S(ats={T1: At(EXCEPTION, 24)}, g=g1, e=(brute_force.RuntimeError, ())),
            }

        elif thread is T1:
            g1 = g({"i": False}, thread, c1)
            g2 = g0

            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 19)}, g=g0),
                S(ats={T1: At(LOCK_RELEASE, 24)}, g=g1),
                S(ats={T1: At(EXCEPTION, 25)}, g=g2, e=(Exception, ())),
            }

        elif thread is T2:
            p2 = At(SET, 13)
            g1 = g({"i": True}, thread, c1)

            _ = brute_force.RuntimeError
            expected_states = {
                S(ats={T1: At(GET, 21), T2: ()}, g=g0),
                S(ats={T1: At(GET, 21), T2: At(LOCK_ACQUIRE, 11)}, g=g0),
                S(ats={T1: At(GET, 21), T2: p2}, g=g({"i": False}, thread, c1)),
                S(ats={T1: At(GET, 21), T2: p2}, g=g1),
                S(ats={T1: At(LOCK_RELEASE, 24), T2: p2}, g=g1),
                S(ats={T1: At(EXCEPTION, 24), T2: p2}, g=g1, e=(_, ())),
            }

        else:
            raise Exception

        _ = {T1: {"f1"}, T2: {"f2"}} if thread is T2 else {T1: {"f1"}}
        _test(code_string, _, S(g=g0), expected_states, reset_name="reset")


def test_context():
    l1 = brute_force.RLock("l1")

    _ = list(itertools.product([False, True], [{}, {T2: False}, {T2: True}]))
    _ = itertools.product([None, T1, T2], [(None, None)] + _)
    for thread, (condition_lock, condition_threads) in _:
        condition_string = (
            "from brute_force import Condition as C, ConditionState as CS, RLockState"
            " as LS, Thread; import frozendict as f;"
            ' brute_force.state.global_objects[C(id="c1")] ='
            f" CS(lock={'LS(thread=None, count=0)' if condition_lock else 'None'},"
            f" threads=f.frozendict({repr(condition_threads)}))"
        )
        lock_string = {
            None: 'brute_force.RLock("l1")',
            False: (
                'brute_force.Condition("c1", lock=brute_force.RLock("l1"));'
                f" {condition_string}"
            ),
            True: f'brute_force.Condition("c1"); {condition_string}',
        }[condition_lock]

        def g(global_objects, thread, count):
            if condition_lock is None:
                global_objects[l1] = LS(thread, count)

            elif condition_lock:
                c1 = brute_force.Condition("c1")
                global_objects[c1] = brute_force.ConditionState(
                    lock=LS(thread, count),
                    threads=frozendict.frozendict(condition_threads),
                )

            else:
                c1 = brute_force.Condition("c1")
                _ = brute_force.ConditionState(
                    lock=None, threads=frozendict.frozendict(condition_threads)
                )
                global_objects.update({c1: _, l1: LS(thread, count)})

            return global_objects

        code_string = f"""
r_lock = None

def reset(brute_force):
    global r_lock

    r_lock = {lock_string}

def f2(brute_force):
    g = brute_force.state.g()

    r_lock.acquire()  # 11
    while True:
        g.i = True  # 13

def f1(brute_force):
    g = brute_force.state.g()

    if {thread is T1}:
        r_lock.acquire()  # 19
    while {thread is T2}:
        if g.i:  # 21
            break

    with r_lock:  # 24
        brute_force.point("p1")
    raise Exception  # 26
""".strip()

        g0 = g({"i": False}, None, 0)
        c1 = 0 if thread is None else 1

        if thread is None:
            g1 = g0
            g2 = g({"i": False}, T1, 1)

            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 24)}, g=g1),
                S(ats={T1: At("p1", A)}, g=g2),
                S(ats={T1: At(LOCK_RELEASE, 24)}, g=g2),
                S(ats={T1: At(EXCEPTION, 26)}, g=g1, e=(Exception, ())),
            }

        elif thread is T1:
            g1 = g({"i": False}, thread, c1)
            g2 = g({"i": False}, T1, 2)

            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 19)}, g=g0),
                S(ats={T1: At(LOCK_ACQUIRE, 24)}, g=g1),
                S(ats={T1: At("p1", A)}, g=g2),
                S(ats={T1: At(LOCK_RELEASE, 24)}, g=g2),
                S(ats={T1: At(EXCEPTION, 26)}, g=g1, e=(Exception, ())),
            }

        elif thread is T2:
            p2 = At(SET, 13)
            g1 = g({"i": True}, thread, c1)

            expected_states = {
                S(ats={T1: At(GET, 21), T2: ()}, g=g0),
                S(ats={T1: At(GET, 21), T2: At(LOCK_ACQUIRE, 11)}, g=g0),
                S(ats={T1: At(GET, 21), T2: p2}, g=g({"i": False}, thread, c1)),
                S(ats={T1: At(GET, 21), T2: p2}, g=g1),
                S(ats={T1: At(LOCK_ACQUIRE, 24), T2: p2}, g=g1),
                S(ats={T1: At(LOCK_WAITING, 24), T2: p2}, g=g1),
            }

        else:
            raise Exception

        _ = {T1: {"f1"}, T2: {"f2"}} if thread is T2 else {T1: {"f1"}}
        _test(code_string, _, S(g=g0), expected_states, reset_name="reset")
