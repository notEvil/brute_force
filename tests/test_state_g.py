import brute_force
import tests.common as t_common


for name, object in vars(t_common).items():
    if name.startswith("__"):
        continue

    globals()[name] = object


def test_setattr():
    code_string = """
def f(brute_force):
    g = brute_force.state.g()

    g.a = True  # 4
    raise Exception  # 5
""".strip()

    g0 = {}
    expected_states = {
        S(ats={T1: At(SET, 4)}, g=g0),
        S(ats={T1: At(EXCEPTION, 5)}, g=dict(a=True), e=(Exception, ())),
    }

    _test(code_string, {T1: {"f"}}, S(g=g0), expected_states)

    g0 = dict(a=False)
    expected_states = {
        S(ats={T1: At(SET, 4)}, g=g0),
        S(ats={T1: At(EXCEPTION, 5)}, g=dict(a=True), e=(Exception, ())),
    }

    _test(code_string, {T1: {"f"}}, S(g=g0), expected_states)


def test_getattr():
    code_string = """
def f(brute_force):
    g = brute_force.state.g()

    assert g.a is False  # 4
    raise Exception  # 5
""".strip()

    g0 = {}
    expected_states = {
        S(ats={T1: At(GET, 4)}, g=g0),
        S(ats={T1: At(EXCEPTION, 4)}, g=g0, e=(brute_force.AttributeError, ())),
    }

    _test(code_string, {T1: {"f"}}, S(), expected_states)

    g0 = dict(a=False)
    expected_states = {
        S(ats={T1: At(GET, 4)}, g=g0),
        S(ats={T1: At(EXCEPTION, 5)}, g=g0, e=(Exception, ())),
    }

    _test(code_string, {T1: {"f"}}, S(g=g0), expected_states)


def test_delattr():
    code_string = """
def f(brute_force):
    g = brute_force.state.g()

    del g.a  # 4
    raise Exception  # 5
""".strip()

    g0 = {}
    expected_states = {
        S(ats={T1: At(DEL, 4)}, g=g0),
        S(ats={T1: At(EXCEPTION, 4)}, g=g0, e=(brute_force.AttributeError, ())),
    }

    _test(code_string, {T1: {"f"}}, S(), expected_states)

    g0 = dict(a=False)
    expected_states = {
        S(ats={T1: At(DEL, 4)}, g=g0),
        S(ats={T1: At(EXCEPTION, 5)}, g={}, e=(Exception, ())),
    }

    _test(code_string, {T1: {"f"}}, S(g=g0), expected_states)


def test_persistence():
    code_string = """
def f(brute_force):
    g = brute_force.state.g()

    g.a = False  # 4
""".strip()

    g0 = {}
    g1 = dict(a=False)
    expected_states = {
        S(ats={T1: At(SET, 4)}, g=g0),
        S(ats={T1: ()}, g=g1),
        S(ats={T1: At(SET, 4)}, g=g1),
    }

    _test(code_string, {T1: {"f"}}, S(g=g0), expected_states)
