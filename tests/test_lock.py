import brute_force
import tests.common as t_common
import itertools


for name, object in vars(t_common).items():
    if name.startswith("__"):
        continue

    globals()[name] = object


def test_init():
    code_string = """
lock = None

def reset(brute_force):
    global lock

    lock = brute_force.Lock("l1")

def f(brute_force):
    pass
""".strip()

    expected_states = {S(ats={T1: ()}, g={brute_force.Lock("l1"): RELEASED})}

    _test(code_string, {T1: {"f"}}, S(), expected_states, reset_name="reset")


def test_acquire():
    l1 = brute_force.Lock("l1")

    _ = [None, {}, {T2: False}, {T2: True}]
    _ = itertools.product([RELEASED, ACQUIRED], [False, True], [-1, 0, 1], _)
    for lock_state, blocking, timeout, condition_threads in _:

        def g(global_objects):
            if condition_threads is not None:
                c1 = brute_force.Condition("c1")
                global_objects[c1] = brute_force.ConditionState(
                    lock=None, threads=frozendict.frozendict(condition_threads)
                )

            return global_objects

        code_string = f"""
lock = None

def reset(brute_force):
    global lock

    lock = {'brute_force.Lock("l1")' if condition_threads is None else f'brute_force.Condition("c1", lock=brute_force.Lock("l1")); from brute_force import Condition as C, ConditionState as CS, Thread; import frozendict as f; brute_force.state.global_objects[C(id="c1")] = CS(lock=None, threads=f.frozendict({repr(condition_threads)}))'}

def f(brute_force):
    if {lock_state is ACQUIRED}:
        lock.acquire()  # 10

    if lock.acquire(blocking={blocking}, timeout={timeout}):  # 12
        brute_force.point("p1")
    else:
        brute_force.point("p2")
    raise Exception  # 16
""".strip()

        g0 = g({l1: RELEASED})
        g1 = g({l1: lock_state})

        if lock_state is RELEASED:
            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 12)}, g=g1),
                S(ats={T1: At("p1", A)}, g=g({l1: ACQUIRED})),
                S(ats={T1: At(EXCEPTION, 16)}, g=g({l1: ACQUIRED}), e=(Exception, ())),
            }

        elif lock_state is ACQUIRED:
            if blocking and timeout == -1:
                _ = At(LOCK_WAITING, 12)
                expected_states = {
                    S(ats={T1: At(LOCK_ACQUIRE, 10)}, g=g0),
                    S(ats={T1: At(LOCK_ACQUIRE, 12)}, g=g1),
                    S(ats={T1: At(LOCK_WAITING, 12)}, g=g1),
                    S(ats={T1: _}, g=g1, e=(brute_force.Deadlock, ())),
                }

            elif not blocking:
                expected_states = {
                    S(ats={T1: At(LOCK_ACQUIRE, 10)}, g=g0),
                    S(ats={T1: At(LOCK_ACQUIRE, 12)}, g=g1),
                    S(ats={T1: At("p2", A)}, g=g1),
                    S(ats={T1: At(EXCEPTION, 16)}, g=g1, e=(Exception, ())),
                }

            else:
                expected_states = {
                    S(ats={T1: At(LOCK_ACQUIRE, 10)}, g=g0),
                    S(ats={T1: At(LOCK_ACQUIRE, 12)}, g=g1),
                    S(ats={T1: At(LOCK_WAITING, 12)}, g=g1),
                    S(ats={T1: At("p2", A)}, g=g1),
                    S(ats={T1: At(EXCEPTION, 16)}, g=g1, e=(Exception, ())),
                }

        else:
            raise Exception

        _test(code_string, {T1: {"f"}}, S(), expected_states, reset_name="reset")


def test_release():
    l1 = brute_force.Lock("l1")

    _ = itertools.product([RELEASED, ACQUIRED], [None, {}, {T2: False}, {T2: True}])
    for lock_state, condition_threads in _:

        def g(global_objects):
            if condition_threads is not None:
                c1 = brute_force.Condition("c1")
                global_objects[c1] = brute_force.ConditionState(
                    lock=None, threads=frozendict.frozendict(condition_threads)
                )

            return global_objects

        code_string = f"""
lock = None

def reset(brute_force):
    global lock

    lock = {'brute_force.Lock("l1")' if condition_threads is None else f'brute_force.Condition("c1", lock=brute_force.Lock("l1")); from brute_force import Condition as C, ConditionState as CS, Thread; import frozendict as f; brute_force.state.global_objects[C(id="c1")] = CS(lock=None, threads=f.frozendict({repr(condition_threads)}))'}

def f(brute_force):
    if {lock_state is ACQUIRED}:
        lock.acquire()  # 10

    lock.release()  # 12
    raise Exception  # 13
""".strip()

        g0 = g({l1: RELEASED})
        g1 = g({l1: lock_state})

        if lock_state is RELEASED:
            expected_states = {
                S(ats={T1: At(LOCK_RELEASE, 12)}, g=g1),
                S(ats={T1: At(EXCEPTION, 12)}, g=g1, e=(brute_force.RuntimeError, ())),
            }

        elif lock_state is ACQUIRED:
            g2 = g0

            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 10)}, g=g0),
                S(ats={T1: At(LOCK_RELEASE, 12)}, g=g1),
                S(ats={T1: At(EXCEPTION, 13)}, g=g2, e=(Exception, ())),
            }

        else:
            raise Exception

        _test(code_string, {T1: {"f"}}, S(), expected_states, reset_name="reset")


def test_locked():
    l1 = brute_force.Lock("l1")

    for lock_state in [RELEASED, ACQUIRED]:
        code_string = f"""
lock = None

def reset(brute_force):
    global lock

    lock = brute_force.Lock("l1")

def f(brute_force):
    if {lock_state is ACQUIRED}:
        lock.acquire()  # 10

    assert lock.locked() is {lock_state is ACQUIRED}  # 12
    raise Exception  # 13
""".strip()

        g0 = {l1: RELEASED}
        g1 = {l1: lock_state}

        if lock_state is RELEASED:
            expected_states = {
                S(ats={T1: At(LOCK_LOCKED, 12)}, g=g1),
                S(ats={T1: At(EXCEPTION, 13)}, g=g1, e=(Exception, ())),
            }

        elif lock_state is ACQUIRED:
            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 10)}, g=g0),
                S(ats={T1: At(LOCK_LOCKED, 12)}, g=g1),
                S(ats={T1: At(EXCEPTION, 13)}, g=g1, e=(Exception, ())),
            }

        else:
            raise Exception

        _test(code_string, {T1: {"f"}}, S(), expected_states, reset_name="reset")


def test_context():
    l1 = brute_force.Lock("l1")

    _ = itertools.product([RELEASED, ACQUIRED], [None, {}, {T2: False}, {T2: True}])
    for lock_state, condition_threads in _:

        def g(global_objects):
            if condition_threads is not None:
                c1 = brute_force.Condition("c1")
                global_objects[c1] = brute_force.ConditionState(
                    lock=None, threads=frozendict.frozendict(condition_threads)
                )

            return global_objects

        code_string = f"""
lock = None

def reset(brute_force):
    global lock

    lock = {'brute_force.Lock("l1")' if condition_threads is None else f'brute_force.Condition("c1", lock=brute_force.Lock("l1")); from brute_force import Condition as C, ConditionState as CS, Thread; import frozendict as f; brute_force.state.global_objects[C(id="c1")] = CS(lock=None, threads=f.frozendict({repr(condition_threads)}))'}

def f(brute_force):
    if {lock_state is ACQUIRED}:
        lock.acquire()  # 10

    with lock:  # 12
        brute_force.point("p1")
    raise Exception  # 14
""".strip()

        g0 = g({l1: RELEASED})
        g1 = g({l1: lock_state})

        if lock_state is RELEASED:
            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 12)}, g=g1),
                S(ats={T1: At("p1", A)}, g=g({l1: ACQUIRED})),
                S(ats={T1: At(LOCK_RELEASE, 12)}, g=g({l1: ACQUIRED})),
                S(ats={T1: At(EXCEPTION, 14)}, g=g1, e=(Exception, ())),
            }

        elif lock_state is ACQUIRED:
            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 10)}, g=g0),
                S(ats={T1: At(LOCK_ACQUIRE, 12)}, g=g1),
                S(ats={T1: At(LOCK_WAITING, 12)}, g=g1),
                S(ats={T1: At(LOCK_WAITING, 12)}, g=g1, e=(brute_force.Deadlock, ())),
            }

        else:
            raise Exception

        _test(code_string, {T1: {"f"}}, S(), expected_states, reset_name="reset")
