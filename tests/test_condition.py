import brute_force
import tests.common as t_common
import frozendict
import functools
import itertools


for name, object in vars(t_common).items():
    if name.startswith("__"):
        continue

    globals()[name] = object


def LS(thread, count):
    return brute_force.RLockState(thread=thread, count=count)


class _G:
    def __init__(self, lock):
        super().__init__()

        self.lock = lock

        self._condition = brute_force.Condition("c1")
        if lock is not None:
            self._lock = lock("l1")

    def __call__(self, global_objects, l=None, t=None):
        t = frozendict.frozendict({} if t is None else t)

        if self.lock is None:
            global_objects[self._condition] = brute_force.ConditionState(
                lock=LS(*(None, 0) if l is None else l), threads=t
            )

        elif self.lock is brute_force.Lock:
            _ = brute_force.ConditionState(lock=None, threads=t)
            _ = {self._lock: RELEASED if l is None else l, self._condition: _}
            global_objects.update(_)

        elif self.lock is brute_force.RLock:
            _ = brute_force.ConditionState(lock=None, threads=t)
            _ = {self._lock: LS(*(None, 0) if l is None else l), self._condition: _}
            global_objects.update(_)

        else:
            raise Exception

        return global_objects


def test_init():
    for lock in [None, brute_force.Lock, brute_force.RLock]:
        g = _G(lock)

        code_string = f"""
condition = None

def reset(brute_force):
    global condition

    condition = brute_force.Condition("c1", lock={'None' if lock is None else f'brute_force.{lock.__name__}("l1")'})

def f(brute_force):
    pass
""".strip()

        expected_states = {S(ats={T1: ()}, g=g({}))}

        _test(code_string, {T1: {"f"}}, S(), expected_states, reset_name="reset")


def test_wait():
    for lock, thread, timeout in itertools.product(
        [None, brute_force.Lock, brute_force.RLock], [None, False, True], [None, 0, 1]
    ):
        g = _G(lock)

        code_string = f"""
import brute_force as bf_module
import frozendict

condition = None

def reset(brute_force):
    global condition

    condition = brute_force.Condition("c1", lock={'None' if lock is None else f'brute_force.{lock.__name__}("l1")'})

def f2(brute_force):
    with condition:
        condition.wait()  # 13
    raise Exception

def f1(brute_force):
    with condition:  # 17
        condition.notify()  # 18
    if {thread is not None}:
        brute_force.point()  # 20
        if brute_force.state.global_objects[bf_module.Condition("c1")] != bf_module.ConditionState(lock={'bf_module.RLockState(thread=None, count=0)' if lock is None else None}, threads=frozendict.frozendict({{bf_module.Thread(id='t2'): {thread}}})):
            raise Exception  # 22

    with condition:  # 24
        assert condition.wait(timeout={timeout}) is False  # 25
    raise Exception  # 26
""".strip()

        l1 = ACQUIRED if lock is brute_force.Lock else (T1, 1)
        g0 = g({})
        g1 = g({}, l=l1, t=None if thread is None else {T2: thread})

        if thread is None:
            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 17)}, g=g0),
                S(ats={T1: At(CONDITION_NOTIFY, 18)}, g=g({}, l=l1)),
                S(ats={T1: At(LOCK_RELEASE, 17)}, g=g({}, l=l1)),
                S(ats={T1: At(LOCK_ACQUIRE, 24)}, g=g0),
                S(ats={T1: At(CONDITION_WAIT, 25)}, g=g1),
                S(ats={T1: At(CONDITION_WAITING, 25)}, g=g({}, t={T1: False})),
            }

            if timeout is None:
                _ = At(CONDITION_WAITING, 25)
                _ = S(ats={T1: _}, g=g({}, t={T1: False}), e=(brute_force.Deadlock, ()))
                expected_states.update({_})

            else:
                _ = S(ats={T1: At(EXCEPTION, 26)}, g=g({}), e=(Exception, ()))
                expected_states.update({S(ats={T1: At(LOCK_RELEASE, 24)}, g=g1), _})

        else:
            p2 = At(CONDITION_WAITING, 13)

            _ = At(CONDITION_WAITING, 25)
            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 24), T2: p2}, g=g({}, t={T2: thread})),
                S(ats={T1: At(CONDITION_WAIT, 25), T2: p2}, g=g1),
                S(ats={T1: _, T2: p2}, g=g({}, t={T1: False, T2: thread})),
            }

            if timeout is None:
                if not thread:
                    _ = S(
                        ats={T1: At(CONDITION_WAITING, 25), T2: p2},
                        g=g({}, t={T1: False, T2: thread}),
                        e=(brute_force.Deadlock, ()),
                    )
                    expected_states.update({_})

            else:
                _ = At(EXCEPTION, 26)
                _ = S(ats={T1: _, T2: p2}, g=g({}, t={T2: thread}), e=(Exception, ()))
                _ = {S(ats={T1: At(LOCK_RELEASE, 24), T2: p2}, g=g1), _}
                expected_states.update(_)

        _ = {T1: {"f1"}} if thread is None else {T1: {"f1"}, T2: {"f2"}}
        states = _test(code_string, _, S(), None, reset_name="reset")

        if thread is None:
            _match(states, expected_states, states, code_string)

        else:
            _ = (
                lambda state: len(state.ats[T1]) == 1
                and state.ats[T1][0].line_number not in [17, 18, 20, 22]
                and state.ats[T2] is not None
                and state.ats[T2][0].line_number == 13
                and state.ats[T2][0].id == CONDITION_WAITING
            )
            _match(filter(_, states), expected_states, states, code_string)


def test_notify():
    _ = [None, brute_force.Lock, brute_force.RLock]
    for lock, thread, n in itertools.product(_, [None, False, True], [0, 1]):
        g = _G(lock)

        code_string = f"""
import brute_force as bf_module
import frozendict

condition = None

def reset(brute_force):
    global condition

    condition = brute_force.Condition("c1", lock={'None' if lock is None else f'brute_force.{lock.__name__}("l1")'})

def f2(brute_force):
    with condition:
        condition.wait()  # 13
    raise Exception

def f1(brute_force):
    with condition:  # 17
        condition.notify()  # 18
    if {thread is not None}:
        brute_force.point()  # 20
        if brute_force.state.global_objects[bf_module.Condition("c1")] != bf_module.ConditionState(lock={'bf_module.RLockState(thread=None, count=0)' if lock is None else None}, threads=frozendict.frozendict({{bf_module.Thread(id='t2'): {thread}}})):
            raise Exception  # 22

    with condition:  # 24
        condition.notify(n={n})  # 25
    raise Exception  # 26
""".strip()

        l1 = ACQUIRED if lock is brute_force.Lock else (T1, 1)
        g0 = g({})
        g1 = g({}, l=l1, t=None if thread is None else {T2: thread})

        if thread is None:
            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 17)}, g=g0),
                S(ats={T1: At(CONDITION_NOTIFY, 18)}, g=g({}, l=l1)),
                S(ats={T1: At(LOCK_RELEASE, 17)}, g=g({}, l=l1)),
                S(ats={T1: At(LOCK_ACQUIRE, 24)}, g=g0),
                S(ats={T1: At(CONDITION_NOTIFY, 25)}, g=g1),
                S(ats={T1: At(LOCK_RELEASE, 24)}, g=g1),
                S(ats={T1: At(EXCEPTION, 26)}, g=g0, e=(Exception, ())),
            }

        else:
            p2 = At(CONDITION_WAITING, 13)
            t2 = {T2: thread or n != 0}

            _ = S(ats={T1: At(EXCEPTION, 26), T2: p2}, g=g({}, t=t2), e=(Exception, ()))
            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 24), T2: p2}, g=g({}, t={T2: thread})),
                S(ats={T1: At(CONDITION_NOTIFY, 25), T2: p2}, g=g1),
                S(ats={T1: At(LOCK_RELEASE, 24), T2: p2}, g=g({}, l=l1, t=t2)),
                _,
            }

        _ = {T1: {"f1"}} if thread is None else {T1: {"f1"}, T2: {"f2"}}
        states = _test(code_string, _, S(), None, reset_name="reset")

        if thread is None:
            _match(states, expected_states, states, code_string)

        else:
            _ = (
                lambda state: len(state.ats[T1]) == 1
                and state.ats[T1][0].line_number not in [17, 18, 20, 22]
                and len(state.ats[T2]) == 1
                and state.ats[T2][0].line_number == 13
                and state.ats[T2][0].id == CONDITION_WAITING
            )
            _match(filter(_, states), expected_states, states, code_string)


def test_notify_all():
    _ = [None, brute_force.Lock, brute_force.RLock]
    for lock, thread in itertools.product(_, [None, False, True]):
        g = _G(lock)

        code_string = f"""
import brute_force as bf_module
import frozendict

condition = None

def reset(brute_force):
    global condition

    condition = brute_force.Condition("c1", lock={'None' if lock is None else f'brute_force.{lock.__name__}("l1")'})

def f2(brute_force):
    with condition:
        condition.wait()  # 13
    raise Exception

def f1(brute_force):
    with condition:  # 17
        condition.notify()  # 18
    if {thread is not None}:
        brute_force.point()  # 20
        if brute_force.state.global_objects[bf_module.Condition("c1")] != bf_module.ConditionState(lock={'bf_module.RLockState(thread=None, count=0)' if lock is None else None}, threads=frozendict.frozendict({{bf_module.Thread(id='t2'): {thread}}})):
            raise Exception  # 22

    with condition:  # 24
        condition.notify_all()  # 25
    raise Exception  # 26
""".strip()

        l1 = ACQUIRED if lock is brute_force.Lock else (T1, 1)
        g0 = g({})
        g1 = g({}, l=l1, t=None if thread is None else {T2: thread})

        if thread is None:
            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 17)}, g=g0),
                S(ats={T1: At(CONDITION_NOTIFY, 18)}, g=g({}, l=l1)),
                S(ats={T1: At(LOCK_RELEASE, 17)}, g=g({}, l=l1)),
                S(ats={T1: At(LOCK_ACQUIRE, 24)}, g=g0),
                S(ats={T1: At(CONDITION_NOTIFY_ALL, 25)}, g=g1),
                S(ats={T1: At(LOCK_RELEASE, 24)}, g=g1),
                S(ats={T1: At(EXCEPTION, 26)}, g=g0, e=(Exception, ())),
            }

        else:
            p2 = At(CONDITION_WAITING, 13)
            t2 = {T2: True}

            _ = S(ats={T1: At(EXCEPTION, 26), T2: p2}, g=g({}, t=t2), e=(Exception, ()))
            expected_states = {
                S(ats={T1: At(LOCK_ACQUIRE, 24), T2: p2}, g=g({}, t={T2: thread})),
                S(ats={T1: At(CONDITION_NOTIFY_ALL, 25), T2: p2}, g=g1),
                S(ats={T1: At(LOCK_RELEASE, 24), T2: p2}, g=g({}, l=l1, t=t2)),
                _,
            }

        _ = {T1: {"f1"}} if thread is None else {T1: {"f1"}, T2: {"f2"}}
        states = _test(code_string, _, S(), None, reset_name="reset")

        if thread is None:
            _match(states, expected_states, states, code_string)

        else:
            _ = (
                lambda state: len(state.ats[T1]) == 1
                and state.ats[T1][0].line_number not in [17, 18, 20, 22]
                and len(state.ats[T2]) == 1
                and state.ats[T2][0].line_number == 13
                and state.ats[T2][0].id == CONDITION_WAITING
            )
            _match(filter(_, states), expected_states, states, code_string)
