import brute_force
import tests.common as t_common


for name, object in vars(t_common).items():
    if name.startswith("__"):
        continue

    globals()[name] = object


def test_init():
    code_string = """
def f(brute_force):
    brute_force.point("p1")
    l = brute_force.state.l()
    brute_force.point("p2")
""".strip()

    expected_states = {
        S(ats={T1: At("p1", A)}),
        S(ats={T1: At("p2", A)}, l={(T1, "f"): {}}),
    }

    _test(code_string, {T1: {"f"}}, S(), expected_states)

    code_string = """
def f1(brute_force):
    f2(1, brute_force)

def f2(a, brute_force):
    l = brute_force.state.l()

    l.a = a
    brute_force.point("p1")

    if 0 < a:
        f2(a - 1, brute_force)
""".strip()

    expected_states = {
        S(ats={T1: (P(None, A), P("p1", A))}, l={(T1, "f1", "f2"): dict(a=1)}),
        S(
            ats={T1: (P(None, A), P(None, A), P("p1", A))},
            l={(T1, "f1", "f2"): dict(a=1), (T1, "f1", "f2", "f2"): dict(a=0)},
        ),
    }

    _test(code_string, {T1: {"f1"}}, S(), expected_states)


def test_setattr():
    code_string = """
def f(brute_force):
    l = brute_force.state.l()

    brute_force.point("p1")
    l.a = False
    brute_force.point("p2")
""".strip()

    expected_states = {
        S(ats={T1: At("p1", A)}, l={(T1, "f"): {}}),
        S(ats={T1: At("p2", A)}, l={(T1, "f"): dict(a=False)}),
    }

    _test(code_string, {T1: {"f"}}, S(), expected_states)

    code_string = """
def f(brute_force):
    l = brute_force.state.l()

    l.a = False

    brute_force.point("p1")
    l.a = True
    brute_force.point("p2")
""".strip()

    expected_states = {
        S(ats={T1: At("p1", A)}, l={(T1, "f"): dict(a=False)}),
        S(ats={T1: At("p2", A)}, l={(T1, "f"): dict(a=True)}),
    }

    _test(code_string, {T1: {"f"}}, S(), expected_states)


def test_getattr():
    code_string = """
def f(brute_force):
    l = brute_force.state.l()

    l.a  # 4
""".strip()

    _ = brute_force.AttributeError
    expected_states = {S(ats={T1: At(EXCEPTION, 4)}, l={(T1, "f"): {}}, e=(_, ()))}

    _test(code_string, {T1: {"f"}}, S(), expected_states)

    code_string = """
def f(brute_force):
    l = brute_force.state.l()

    l.a = False
    assert l.a is False
""".strip()

    expected_states = set()

    _test(code_string, {T1: {"f"}}, S(), expected_states)


def test_delattr():
    code_string = """
def f(brute_force):
    l = brute_force.state.l()

    brute_force.point("p1")
    del l.a  # 5
    brute_force.point("p2")
""".strip()

    l0 = {(T1, "f"): {}}
    expected_states = {
        S(ats={T1: At("p1", A)}, l=l0),
        S(ats={T1: At(EXCEPTION, 5)}, l=l0, e=(brute_force.AttributeError, ())),
    }

    _test(code_string, {T1: {"f"}}, S(), expected_states)

    code_string = """
def f(brute_force):
    l = brute_force.state.l()

    l.a = False

    brute_force.point("p1")
    del l.a
    brute_force.point("p2")
""".strip()

    _ = S(ats={T1: At("p1", A)}, l={(T1, "f"): dict(a=False)})
    expected_states = {_, S(ats={T1: At("p2", A)}, l={(T1, "f"): {}})}

    _test(code_string, {T1: {"f"}}, S(), expected_states)
