import brute_force
import tests.common as t_common
import frozendict
import itertools


for name, object in vars(t_common).items():
    if name.startswith("__"):
        continue

    globals()[name] = object


def test_1_trivial():
    code_string = """
def f(brute_force):
    pass
""".strip()

    expected_states = set()

    _test(code_string, {T1: {"f"}}, S(), expected_states)


def test_1_point():
    code_string = """
def f1(brute_force):
    brute_force.point("p1")

def f2(brute_force):
    brute_force.point("p2")
""".strip()

    expected_states = {S(ats={T1: At("p1", A)}), S(ats={T1: At("p2", A)})}

    _test(code_string, {T1: {"f1", "f2"}}, S(), expected_states)


def test_1_globals():
    code_string = """
def f(brute_force):
    g = brute_force.state.g()
    if g.a:  # 3
        g.a = False  # 4
    else:
        g.a = True  # 6
""".strip()

    expected_states = {
        S(ats={T1: At(GET, 3)}, g=dict(a=False)),
        S(ats={T1: At(SET, 6)}, g=dict(a=False)),
        S(ats={T1: At(GET, 3)}, g=dict(a=True)),
        S(ats={T1: At(SET, 4)}, g=dict(a=True)),
        S(ats={T1: ()}, g=dict(a=True)),
    }

    _test(code_string, {T1: {"f"}}, S(g=dict(a=False)), expected_states)


def test_1_locals():
    code_string = """
def f1(brute_force):
    l = brute_force.state.l()
    brute_force.point("p1")
    l.a = False
    brute_force.point("p2")

def f2(brute_force):
    def _f2(a=0):
        l = brute_force.state.l()
        brute_force.point("p3")
        l.a = a
        if a < 1:
            _f2(a=a + 1)

    _f2()
""".strip()

    _ = (P(None, A), P(None, A), P("p3", A))
    expected_states = {
        S(ats={T1: At("p1", A)}, l={(T1, "f1"): {}}),
        S(ats={T1: At("p2", A)}, l={(T1, "f1"): dict(a=False)}),
        S(ats={T1: (P(None, A), P("p3", A))}, l={(T1, "f2", "_f2"): {}}),
        S(ats={T1: _}, l={(T1, "f2", "_f2"): dict(a=0), (T1, "f2", "_f2", "_f2"): {}}),
    }

    _test(code_string, {T1: {"f1", "f2"}}, S(), expected_states)


def test_1_inplace():
    code_string = """
def f(brute_force):
    g = brute_force.state.g()
    g.a = 0  # 3
    g.a += 1  # 4
""".strip()

    expected_states = {
        S(ats={T1: At(SET, 3)}),
        S(ats={T1: At(GET, 4)}, g=dict(a=0)),
        S(ats={T1: At(SET, 4)}, g=dict(a=0)),
        S(ats={T1: At(SET, 3)}, g=dict(a=1)),
        S(ats={T1: ()}, g=dict(a=1)),
    }

    _test(code_string, {T1: {"f"}}, S(), expected_states)


def test_hasattr():
    code_string = """
def f(brute_force):
    g = brute_force.state.g()
    hasattr(g, "a")  # 3
""".strip()

    expected_states = {S(ats={T1: At(GET, 3)})}

    _test(code_string, {T1: {"f"}}, S(), expected_states)


def test_1_exception():
    code_string = """
class _Exception(Exception):
    pass

def f(brute_force):
    raise _Exception
""".strip()

    globals = {}
    exec(compile(code_string, "<string>", "exec"), globals)

    states = brute_force.process_code(S(), {T1: {globals["f"]}})

    _ = {S(ats={T1: At(EXCEPTION, A)}, e=(globals["_Exception"], ()))}
    _match(states, _, states, code_string)


def test_2_trivial():
    code_string = """
def f(brute_force):
    pass
""".strip()

    expected_states = set()

    _test(code_string, {T1: {"f"}}, S(), expected_states)


def test_2_point():
    code_string = """
def f1(brute_force):
    brute_force.point("p1")

def f2(brute_force):
    brute_force.point("p2")
""".strip()

    _ = {
        S(ats={T1: (), T2: At("p1", A)}),
        S(ats={T1: (), T2: At("p2", A)}),
        S(ats={T1: At("p1", A), T2: At("p1", A)}),
        S(ats={T1: At("p1", A), T2: At("p2", A)}),
        S(ats={T1: At("p2", A), T2: At("p2", A)}),
    }
    expected_states = sym(_)

    _test(code_string, {T1: {"f1", "f2"}, T2: {"f1", "f2"}}, S(), expected_states)

    code_string = """
def f1(brute_force):
    brute_force.point("p1")

def f2(brute_force):
    brute_force.point("p2")
""".strip()

    expected_states = {
        S(ats={T1: (), T2: At("p2", A)}),
        S(ats={T1: At("p1", A), T2: ()}),
        S(ats={T1: At("p1", A), T2: At("p2", A)}),
    }

    _test(code_string, {T1: {"f1"}, T2: {"f2"}}, S(), expected_states)


def test_2_exception():
    code_string = """
class _Exception(Exception):
    pass

def f(brute_force):
    raise _Exception
""".strip()

    globals = {}
    exec(compile(code_string, "<string>", "exec"), globals)

    functions = {globals["f"]}
    states = brute_force.process_code(S(), {T1: functions, T2: functions})

    _ = {S(ats={T1: At(EXCEPTION, A), T2: ()}, e=(globals["_Exception"], ()))}
    _match(states, _, states, code_string)


def test_1_benchmark_1():
    bits = 16

    functions = "\n\n".join(f"""
def _f{index}(g):
    if g.b{index}:
        g.b{index} = False
        _f{index + 1}(g)
    else:
        g.b{index} = True
""".strip() for index in range(bits))

    code_string = f"""
def f(brute_force):
    g = brute_force.state.g()
    _f0(g)

{functions}

def _f{bits}(g):
    pass
""".strip()

    _ = S(g={f"b{index}": False for index in range(bits)})
    states = _test(code_string, {T1: {"f"}}, _, None)

    _ = {state for state in states if state.exception is not None}
    _match(_, set(), states, code_string)

    names = [f"b{index}" for index in range(bits)]
    _ = len({tuple(state.global_objects[name] for name in names) for state in states})
    assert _ == 2**bits


def test_1_benchmark_2():
    """
    - way slower than test_1_benchmark_1
      - due to branching
    """

    bits = 8

    code_string = "\n\n".join(f"""
def f{index}(brute_force):
    g = brute_force.state.g()
    g.b{index} = not g.b{index}
""".strip() for index in range(bits))

    _ = S(g={f"b{index}": False for index in range(bits)})
    states = _test(code_string, {T1: {f"f{index}" for index in range(bits)}}, _, None)

    _ = {state for state in states if state.exception is not None}
    _match(_, set(), states, code_string)

    names = [f"b{index}" for index in range(bits)]
    _ = len({tuple(state.global_objects[name] for name in names) for state in states})
    assert _ == 2**bits


def test_2_lock_default():
    def _direct(lock_type, g0, g1, g2):
        code_string = f"""
lock = None

def reset(brute_force):
    global lock

    lock = brute_force.{lock_type}("l1")

def f(brute_force):
    if lock.acquire():
        lock.release()
    else:
        brute_force.point("p1")
""".strip()

        expected_states = {
            S(ats={T1: (), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(
                ats={T1: (), T2: At(LOCK_WAITING, A)}, g=g0
            ),  # T1 -> LOCK_RELEASE, T2 -> LOCK_WAITING, T1 -> None
            S(ats={T1: (), T2: At(LOCK_RELEASE, A)}, g=g2),
            # S(ats={T1: (), T2: At("p1", A)}),
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(
                ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_WAITING, A)}, g=g0
            ),  # T1 -> LOCK_RELEASE, T2 -> LOCK_WAITING, T1 -> LOCK_ACQUIRE
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_RELEASE, A)}, g=g2),
            # S(ats={T1: At(LOCK_ACQUIRE, A), T2: At("p1", A)}),
            # S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_WAITING, A)}),
            S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_RELEASE, A)}, g=g2),
            # S(ats={T1: At(LOCK_WAITING, A), T2: At("p1", A)}),
            # S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_RELEASE, A)}),
            # S(ats={T1: At(LOCK_RELEASE, A), T2: At("p1", A)}),
            # S(ats={T1: At("p1", A), T2: At("p1", A)}),
            # === sym ===
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: ()}, g=g0),
            S(ats={T1: At(LOCK_WAITING, A), T2: ()}, g=g0),
            S(ats={T1: At(LOCK_RELEASE, A), T2: ()}, g=g1),
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_ACQUIRE, A)}, g=g1),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_WAITING, A)}, g=g1),
        }

        _ = {T1: {"f"}, T2: {"f"}}
        _test(code_string, _, S(g=g0), expected_states, reset_name="reset")

    l1 = brute_force.Lock("l1")
    _direct("Lock", {l1: RELEASED}, {l1: ACQUIRED}, {l1: ACQUIRED})

    l1 = brute_force.RLock("l1")
    _direct(
        "RLock",
        {l1: brute_force.RLockState(thread=None, count=0)},
        {l1: brute_force.RLockState(thread=T1, count=1)},
        {l1: brute_force.RLockState(thread=T2, count=1)},
    )

    def _context(lock_type, g0, g1, g2):
        code_string = f"""
lock = None

def reset(brute_force):
    global lock

    lock = brute_force.{lock_type}("l1")

def f(brute_force):
    with lock:
        brute_force.point("p1")
""".strip()

        expected_states = {
            S(ats={T1: (), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(
                ats={T1: (), T2: At(LOCK_WAITING, A)}, g=g0
            ),  # T1 -> p1, T2 -> LOCK_WAITING, T1 -> None
            S(ats={T1: (), T2: At("p1", A)}, g=g2),
            S(ats={T1: (), T2: At(LOCK_RELEASE, A)}, g=g2),
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(
                ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_WAITING, A)}, g=g0
            ),  # T1 -> p1, T2 -> LOCK_WAITING, T1 -> LOCK_ACQUIRE
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At("p1", A)}, g=g2),
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_RELEASE, A)}, g=g2),
            # S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_WAITING, A)}),
            S(ats={T1: At(LOCK_WAITING, A), T2: At("p1", A)}, g=g2),
            S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_RELEASE, A)}, g=g2),
            # S(ats={T1: At("p1", A), T2: At("p1", A)}),
            # S(ats={T1: At("p1", A), T2: At(LOCK_RELEASE, A)}),
            # S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_RELEASE, A)}),
            # === sym ===
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: ()}, g=g0),
            S(ats={T1: At(LOCK_WAITING, A), T2: ()}, g=g0),
            S(ats={T1: At("p1", A), T2: ()}, g=g1),
            S(ats={T1: At(LOCK_RELEASE, A), T2: ()}, g=g1),
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At("p1", A), T2: At(LOCK_ACQUIRE, A)}, g=g1),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_ACQUIRE, A)}, g=g1),
            S(ats={T1: At("p1", A), T2: At(LOCK_WAITING, A)}, g=g1),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_WAITING, A)}, g=g1),
        }

        _ = {T1: {"f"}, T2: {"f"}}
        _test(code_string, _, S(g=g0), expected_states, reset_name="reset")

    l1 = brute_force.Lock("l1")
    _context("Lock", {l1: RELEASED}, {l1: ACQUIRED}, {l1: ACQUIRED})

    l1 = brute_force.RLock("l1")
    _context(
        "RLock",
        {l1: brute_force.RLockState(thread=None, count=0)},
        {l1: brute_force.RLockState(thread=T1, count=1)},
        {l1: brute_force.RLockState(thread=T2, count=1)},
    )


def test_2_lock_not_blocking():
    def _blocking(lock_type, g0, g1, g2):
        code_string = f"""
lock = None

def reset(brute_force):
    global lock

    lock = brute_force.{lock_type}("l1")

def f(brute_force):
    if lock.acquire(blocking=False):
        lock.release()
    else:
        brute_force.point("p1")
""".strip()

        _ = S(ats={T1: At(LOCK_ACQUIRE, A), T2: At("p1", A)}, g=g0)
        expected_states = {
            S(ats={T1: (), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: (), T2: At(LOCK_RELEASE, A)}, g=g2),
            S(
                ats={T1: (), T2: At("p1", A)}, g=g0
            ),  # T1 -> LOCK_RELEASE, T2 -> p1, T1 -> None
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_RELEASE, A)}, g=g2),
            _,  # T1 -> LOCK_RELEASE, T2 -> p1, T1 -> LOCK_ACQUIRE
            # S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_RELEASE, A)}),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At("p1", A)}, g=g1),
            # S(ats={T1: At("p1", A), T2: At("p1", A)}),
            # === sym ===
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: ()}, g=g0),
            S(ats={T1: At(LOCK_RELEASE, A), T2: ()}, g=g1),
            S(ats={T1: At("p1", A), T2: ()}, g=g0),
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_ACQUIRE, A)}, g=g1),
            S(ats={T1: At("p1", A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At("p1", A), T2: At(LOCK_RELEASE, A)}, g=g2),
        }

        _ = {T1: {"f"}, T2: {"f"}}
        _test(code_string, _, S(g=g0), expected_states, reset_name="reset")

    l1 = brute_force.Lock("l1")
    _blocking("Lock", {l1: RELEASED}, {l1: ACQUIRED}, {l1: ACQUIRED})

    l1 = brute_force.RLock("l1")
    _blocking(
        "RLock",
        {l1: brute_force.RLockState(thread=None, count=0)},
        {l1: brute_force.RLockState(thread=T1, count=1)},
        {l1: brute_force.RLockState(thread=T2, count=1)},
    )

    def _timeout(lock_type, g0, g1, g2):
        code_string = f"""
lock = None

def reset(brute_force):
    global lock

    lock = brute_force.{lock_type}("l1")

def f(brute_force):
    if lock.acquire(timeout=0):
        lock.release()
    else:
        brute_force.point("p1")
""".strip()

        _ = S(ats={T1: At(LOCK_ACQUIRE, A), T2: At("p1", A)}, g=g0)
        expected_states = {
            S(ats={T1: (), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: (), T2: At(LOCK_WAITING, A)}, g=g0),
            S(ats={T1: (), T2: At(LOCK_RELEASE, A)}, g=g2),
            S(
                ats={T1: (), T2: At("p1", A)}, g=g0
            ),  # T1 -> LOCK_RELEASE, T2 -> p1, T1 -> None
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_WAITING, A)}, g=g0),
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_RELEASE, A)}, g=g2),
            _,  # T1 -> LOCK_RELEASE, T2 -> p1, T1 -> LOCK_ACQUIRE
            # S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_WAITING, A)}),
            S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_RELEASE, A)}, g=g2),
            # S(ats={T1: At(LOCK_WAITING, A), T2: At("p1")}),
            # S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_RELEASE, A)}),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At("p1", A)}, g=g1),
            # S(ats={T1: At("p1", A), T2: At("p1", A)}),
            # === sym ===
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: ()}, g=g0),
            S(ats={T1: At(LOCK_WAITING, A), T2: ()}, g=g0),
            S(ats={T1: At(LOCK_RELEASE, A), T2: ()}, g=g1),
            S(ats={T1: At("p1", A), T2: ()}, g=g0),
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_ACQUIRE, A)}, g=g1),
            S(ats={T1: At("p1", A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_WAITING, A)}, g=g1),
            S(ats={T1: At("p1", A), T2: At(LOCK_RELEASE, A)}, g=g2),
        }

        _ = {T1: {"f"}, T2: {"f"}}
        _test(code_string, _, S(g=g0), expected_states, reset_name="reset")

    l1 = brute_force.Lock("l1")
    _timeout("Lock", {l1: RELEASED}, {l1: ACQUIRED}, {l1: ACQUIRED})

    l1 = brute_force.RLock("l1")
    _timeout(
        "RLock",
        {l1: brute_force.RLockState(thread=None, count=0)},
        {l1: brute_force.RLockState(thread=T1, count=1)},
        {l1: brute_force.RLockState(thread=T2, count=1)},
    )


def test_2_lock_timeout():
    def _direct(lock_type, g0, g1, g2):
        code_string = f"""
lock = None

def reset(brute_force):
    global lock

    lock = brute_force.{lock_type}("l1")

def f(brute_force):
    if lock.acquire(timeout=1):
        lock.release()
    else:
        brute_force.point("p1")
""".strip()

        expected_states = {
            S(ats={T1: (), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(
                ats={T1: (), T2: At(LOCK_WAITING, A)}, g=g0
            ),  # T1 -> LOCK_RELEASE, T2 -> LOCK_WAITING, T1 -> None
            S(ats={T1: (), T2: At(LOCK_RELEASE, A)}, g=g2),
            S(
                ats={T1: (), T2: At("p1", A)}, g=g0
            ),  # T1 -> LOCK_RELEASE, T2 -> p1, T1 -> None
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(
                ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_WAITING, A)}, g=g0
            ),  # T1 -> LOCK_RELEASE, T2 -> LOCK_WAITING, T1 -> LOCK_ACQUIRE
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_RELEASE, A)}, g=g2),
            S(
                ats={T1: At(LOCK_ACQUIRE, A), T2: At("p1", A)}, g=g0
            ),  # T1 -> LOCK_RELEASE, T2 -> p1, T1 -> LOCK_ACQUIRE
            # S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_WAITING, A)}),
            S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_RELEASE, A)}, g=g2),
            # S(ats={T1: At(LOCK_WAITING, A), T2: At("p1", A)}),  # assuming LOCK_WAITING, p1 is impossible, and vice versa
            # S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_RELEASE, A)}),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At("p1", A)}, g=g1),
            # S(ats={T1: At("p1", A), T2: At("p1", A)}),
            # === sym ===
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: ()}, g=g0),
            S(ats={T1: At(LOCK_WAITING, A), T2: ()}, g=g0),
            S(ats={T1: At(LOCK_RELEASE, A), T2: ()}, g=g1),
            S(ats={T1: At("p1", A), T2: ()}, g=g0),
            S(ats={T1: At(LOCK_ACQUIRE, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_WAITING, A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_ACQUIRE, A)}, g=g1),
            S(ats={T1: At("p1", A), T2: At(LOCK_ACQUIRE, A)}, g=g0),
            S(ats={T1: At(LOCK_RELEASE, A), T2: At(LOCK_WAITING, A)}, g=g1),
            S(ats={T1: At("p1", A), T2: At(LOCK_RELEASE, A)}, g=g2),
        }

        _ = {T1: {"f"}, T2: {"f"}}
        _test(code_string, _, S(g=g0), expected_states, reset_name="reset")

    l1 = brute_force.Lock("l1")
    _direct("Lock", {l1: RELEASED}, {l1: ACQUIRED}, {l1: ACQUIRED})

    l1 = brute_force.RLock("l1")
    _direct(
        "RLock",
        {l1: brute_force.RLockState(thread=None, count=0)},
        {l1: brute_force.RLockState(thread=T1, count=1)},
        {l1: brute_force.RLockState(thread=T2, count=1)},
    )


def test_1_lock_deadlock():
    code_string = """
lock = None

def reset(brute_force):
    global lock

    lock = brute_force.Lock("l1")

def f(brute_force):
    with lock:
        with lock:  # 5
            pass
""".strip()

    l1 = brute_force.Lock("l1")

    _ = S(g={l1: RELEASED})
    states = _test(code_string, {T1: {"f"}}, _, None, reset_name="reset")

    _ = At(LOCK_WAITING, 10)
    expected_states = {S(ats={T1: _}, g={l1: ACQUIRED}, e=(brute_force.Deadlock, ()))}

    _ = {state for state in states if state.exception is not None}
    _match(_, expected_states, states, code_string)


def test_2_lock_deadlock():
    def _control(lock_type):
        code_string = f"""
lock_1 = None
lock_2 = None

def reset(brute_force):
    global lock_1
    global lock_2

    lock_1 = brute_force.{lock_type}("l1")
    lock_2 = brute_force.{lock_type}("l2")

def f(brute_force):
    with lock_1, lock_2:
        pass
""".strip()

        _ = {T1: {"f"}, T2: {"f"}}
        states = _test(code_string, _, S(), None, reset_name="reset")

        _ = {state for state in states if state.exception is not None}
        _match(_, set(), states, code_string)

    _control("Lock")
    _control("RLock")

    def _order(lock_type, g0, g12, g21):
        code_string = f"""
lock_1 = None
lock_2 = None

def reset(brute_force):
    global lock_1
    global lock_2

    lock_1 = brute_force.{lock_type}("l1")
    lock_2 = brute_force.{lock_type}("l2")

def f(brute_force):
    with lock_1, lock_2:  # 12
        pass

    with lock_2, lock_1:  # 15
        pass
""".strip()

        _ = {T1: {"f"}, T2: {"f"}}
        states = _test(code_string, _, S(g=g0), None, reset_name="reset")

        e = (brute_force.Deadlock, ())
        expected_states = {
            S(ats={T1: At(LOCK_WAITING, 12), T2: At(LOCK_WAITING, 15)}, g=g12, e=e),
            S(ats={T1: At(LOCK_WAITING, 15), T2: At(LOCK_WAITING, 12)}, g=g21, e=e),
        }

        _ = {state for state in states if state.exception is not None}
        _match(_, expected_states, states, code_string)

    l1 = brute_force.Lock("l1")
    l2 = brute_force.Lock("l2")
    g1 = {l1: ACQUIRED, l2: ACQUIRED}
    _order("Lock", {l1: RELEASED, l2: RELEASED}, g1, g1)

    l1 = brute_force.RLock("l1")
    l2 = brute_force.RLock("l2")
    s0 = brute_force.RLockState(thread=None, count=0)
    s1 = brute_force.RLockState(thread=T1, count=1)
    s2 = brute_force.RLockState(thread=T2, count=1)
    _order("RLock", {l1: s0, l2: s0}, {l1: s1, l2: s2}, {l1: s2, l2: s1})


def test_1_rlock():
    code_string = """
def f(brute_force):
    lock = brute_force.RLock("l1")

    with lock:
        with lock:
            pass
""".strip()

    states = _test(code_string, {T1: {"f"}}, S(), None)

    _ = {state for state in states if state.exception is not None}
    _match(_, set(), states, code_string)


def test_1_lock_state():
    code_string = f"""
lock = None

def reset(brute_force):
    global lock

    lock = brute_force.Lock("l1")

def f(brute_force):
    if lock.locked():
        lock.release()
    else:
        lock.acquire()
""".strip()

    l1 = brute_force.Lock("l1")
    g0 = {l1: RELEASED}
    g1 = {l1: ACQUIRED}

    expected_states = {
        S(ats={T1: At(LOCK_LOCKED, A)}, g=g0),
        S(ats={T1: At(LOCK_ACQUIRE, A)}, g=g0),
        S(ats={T1: ()}, g=g1),
        S(ats={T1: At(LOCK_LOCKED, A)}, g=g1),
        S(ats={T1: At(LOCK_RELEASE, A)}, g=g1),
    }

    _test(code_string, {T1: {"f"}}, S(g=g0), expected_states, reset_name="reset")


def test_1_event():
    e1 = brute_force.Event("e1")
    g0 = {e1: brute_force.EventState(set=False, threads=frozendict.frozendict())}
    _ = brute_force.EventState(set=False, threads=frozendict.frozendict({T1: False}))
    g1 = {e1: _}

    code_string = """
event = None

def reset(brute_force):
    global event

    event = brute_force.Event("e1")

def f(brute_force):
    event.wait()
""".strip()

    expected_states = {
        S(ats={T1: At(EVENT_WAIT, A)}, g=g0),
        S(ats={T1: At(EVENT_WAITING, A)}, g=g1),
        S(ats={T1: At(EVENT_WAITING, A)}, g=g1, e=(brute_force.Deadlock, ())),
    }

    _test(code_string, {T1: {"f"}}, S(g=g0), expected_states, reset_name="reset")

    code_string = """
event = None

def reset(brute_force):
    global event

    event = brute_force.Event("e1")

def f(brute_force):
    assert event.wait(timeout=0) is False
""".strip()

    expected_states = {
        S(ats={T1: At(EVENT_WAIT, A)}, g=g0),
        S(ats={T1: At(EVENT_WAITING, A)}, g=g1),
    }

    _test(code_string, {T1: {"f"}}, S(g=g0), expected_states, reset_name="reset")

    code_string = """
event = None

def reset(brute_force):
    global event

    event = brute_force.Event("e1")

def f(brute_force):
    assert event.wait(timeout=1) is False
""".strip()

    expected_states = {
        S(ats={T1: At(EVENT_WAIT, A)}, g=g0),
        S(ats={T1: At(EVENT_WAITING, A)}, g=g1),
    }

    _test(code_string, {T1: {"f"}}, S(g=g0), expected_states, reset_name="reset")

    code_string = """
event = None

def reset(brute_force):
    global event

    event = brute_force.Event("e1")

def f(brute_force):
    event.set()
    assert event.wait() is True
""".strip()

    g2 = {e1: brute_force.EventState(set=True, threads=frozendict.frozendict())}
    expected_states = {
        S(ats={T1: At(EVENT_SET, A)}, g=g0),
        S(ats={T1: At(EVENT_WAIT, A)}, g=g2),
        S(ats={T1: ()}, g=g2),
        S(ats={T1: At(EVENT_SET, A)}, g=g2),
    }

    _test(code_string, {T1: {"f"}}, S(g=g0), expected_states, reset_name="reset")


def test_point():
    for id, switch_thread in itertools.product([None, "p1"], [False, True]):
        code_string = f"""
def f(brute_force):
    brute_force.point(id={repr(id)}, switch_thread={switch_thread})
""".strip()

        if switch_thread:
            expected_states = {
                S(ats={T1: (), T2: At(id, A)}),
                S(ats={T1: At(id, A), T2: ()}),
                S(ats={T1: At(id, A), T2: At(id, A)}),
            }

        else:
            expected_states = {S(ats={T1: At(id, A), T2: ()})}

        _test(code_string, {T1: {"f"}, T2: {"f"}}, S(), expected_states)
