import brute_force
import frozendict


class _FrozenDict(frozendict.frozendict):
    def __repr__(self):
        return f"f{super().__repr__()[12:-1]}"


frozendict.frozendict = _FrozenDict


for point_id in brute_force.PointIds:
    globals()[point_id.name] = point_id


RELEASED = brute_force.RELEASED
ACQUIRED = brute_force.ACQUIRED


class _Var:
    def __init__(self, set=None):
        super().__init__()

        self.set = set

    def __hash__(self):
        return super().__hash__()

    def __eq__(self, object):
        if isinstance(object, _Var):
            return object is self

        if self.set is not None and object not in self.set:
            return False

        return True

    def __repr__(self):
        attributes = {}
        for name in ["set"]:
            object = getattr(self, name)
            if object is not None:
                attributes[name] = object

        _ = ", ".join(f"{name}={repr(object)}" for name, object in attributes.items())
        return f"Var({_})"


A = _Var()
T1 = brute_force.Thread(id="t1")
T2 = brute_force.Thread(id="t2")


def _test(code_string, entry_names, initial_state, expected_states, reset_name=None):
    globals = {}
    exec(compile(code_string, "<string>", "exec"), globals)

    _ = entry_names.items()
    states = brute_force.process_code(
        initial_state,
        {thread: {globals[name] for name in names} for thread, names in _},
        reset_function=None if reset_name is None else globals[reset_name],
    )

    if expected_states is not None:
        _match(states, expected_states, states, code_string)

    return states


def _match(states, expected_states, state_dictionary, code_string):
    expected_states = list(expected_states)

    for state in states:
        try:
            index = expected_states.index(state)

        except ValueError:
            _ = brute_force.format_history(
                state, state_dictionary, code_string=code_string
            )
            raise Exception(f"unexpected state:\n{_}")

        del expected_states[index]

    if len(expected_states) != 0:
        _ = "\n\n".join(
            brute_force.format_history(expected_state, {}, code_string=code_string)
            for expected_state in expected_states
        )
        raise Exception(f"missing states:\n{_}")


def S(ats=None, g=None, l=None, e=None):
    _ = brute_force.State(ats=ats, global_objects=g, local_objects=l, exception=e)
    return _.get_frozen()


def P(id, line_number):
    return brute_force.Point(
        id=id, file_name="<string>", line_number=line_number, lasti=A
    )


def At(id, line_number):
    return (P(id, line_number),)


def sym(states):
    for state in list(states):
        _ = S(
            ats={T1: state.ats[T2], T2: state.ats[T1]},
            g=state.global_objects,
            l=state.local_objects,
            e=state.exception,
        )
        states.add(_)

    return states
