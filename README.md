# *brute_force*

is a Python package for exhaustive verification of Python code. Its primary use case is to determine the nonexistence of undesirable states of small programs, usually analogs of specific parts of actual programs, when exposed to multiple threads. It is **not** and never will be capable of verifying actual programs or for any number of threads, see [How](#how).

## Features

- Explicit code points (think breakpoint)
- Globals
- Locals
- Shortest path to state
- Collects exceptions
- Lock, RLock, Event, Condition
- Detects deadlocks

## Example

[./examples/race_trivial.py](./examples/race_trivial.py)

```python
def f(brute_force):
    g = brute_force.state.g()

    g.a += 1
    brute_force.point("p1")

    raise Exception
```

with `a = 0`, two threads and looking for states where both threads are at `"p1"` and `a != 2` yields a single state with history

```
                                    at                                      global
                       Thread(id='T1')                      Thread(id='T2')      a
0  ()                                   ()                                       0
1  (4 | g.a += 1              | GET,)                                             
2  (4 | g.a += 1              | SET,)                                             
3                                       (4 | g.a += 1              | GET,)        
4                                       (4 | g.a += 1              | SET,)        
5                                       (5 | brute_force.point("p..| 'p1',)      1
6  (5 | brute_force.point("p..| 'p1',)                                            
```

Read:
- after `2` steps,
- thread `'T1'` is at line `4` which starts with `g.a += 1` and **before** setting a variable,
- thread `'T2'` is still outside
- and the global variable `a` is still `0`.

## Quickstart

- Get [*Pipenv*](https://github.com/pypa/pipenv) with `python -m pip install pipenv`
- Create virtual environment with `python -m pipenv install -e .[all]`
- Run tests with `python -m pipenv run python -m pytest -v .`
- or examples with `python -m pipenv run python ./examples/{example}.py`
- or the tutorial with `python -m pipenv run python -m jupyter lab ./Tutorial.ipynb`

## Motivation

In general, when multiple threads observe and modify shared state, full protection against race conditions is usually difficult to achieve. Synchronization primitives at critical spots tend to create the illusion that race conditions are solved. It is easy to miss windows for races or introduce new ones when changing code.

If you expect serious consequences in case of errors, you should use formal verification methods to determine whether or not your program works as intended. However, for "small" projects or small solutions, doing that is rather expensive (e.g. time to learn, apply correctly, ...).

I started this project to prove a race condition in [*RPyC*](https://github.com/tomerfiliba-org/rpyc) and its absence after the [fix](https://github.com/tomerfiliba-org/rpyc/pull/531), see [./examples/rpyc_5.3.1.py](./examples/rpyc_5.3.1.py).

## Details

### How

`brute_force.process_code` takes an initial state (`brute_force.State`) and a dictionary mapping threads (`brute_force.Thread`) to entry functions as arguments. It creates actual threads which wait for their turn and let the first thread start. Initially, threads are outside any function. Threads may reach points where they may pass control to another thread (e.g. before observing/modifying the state, waiting for a lock) or points where they have to decide how to proceed (e.g. when outside: choose an entry function, when waiting for a lock: decide if it timed out). Both create branches which will be explored later. When reaching a state that is already known, or an exception isn't handled, execution ends and the threads are reset. Then, the next unexplored branch is executed using a replay stack.

The main disadvantages of this approach are path explosion and that trust in the actual program is traded for trust in the analog and *brute_force*.

## TODO

- Find a (better) name
- Documentation
