import brute_force
import functools


MAX_THREADS = 2


try:
    import rich.console as r_console

except ImportError:
    rich = False

else:
    rich = True
    _ = r_console.Console().print
    print = functools.partial(_, highlight=False, overflow="ignore", crop=False)


def process_code(code_string, threads):
    globals = {}
    exec(code_string, globals)

    T1 = brute_force.Thread(id="T1")

    entry_functions = {T1: {globals["AsyncResult_wait"]}}
    for index in range(1, threads):
        _ = globals["Connection_serve"]
        entry_functions[brute_force.Thread(id=f"T{index + 1}")] = {_}

    _ = brute_force.State(global_objects=dict(is_ready=False))
    states = brute_force.process_code(
        _, entry_functions, reset_function=globals["reset"]
    )

    print(len(states), "states")

    _ = (
        lambda state: (
            state.g().is_ready
            and len(state.ats[T1]) == 1
            and state.ats[T1][0].id == "recv"
        )
        or state.exception is not None
    )
    bad_states = set(filter(_, states))

    if len(bad_states) != 0:
        print("bad state:")
        _ = min(
            bad_states,
            key=lambda state: len(list(brute_force.get_history(state, states))),
        )
        print(brute_force.format_history(_, states, code_string=code_string, rich=rich))


if True:
    print()
    print("rpyc 5.3.1")
    for threads in range(1, MAX_THREADS + 1):
        print()
        print("  threads:", threads)

        _ = """
recv_event = None
recvlock = None

def reset(brute_force):
    global recv_event
    global recvlock

    recv_event = brute_force.Condition("recv_event")
    recvlock = brute_force.Lock("recvlock")


def AsyncResult_wait(brute_force):
    g = brute_force.state.g()

    expired = False

    while not (g.is_ready or expired):
        Connection_serve(brute_force)


def Connection_serve(brute_force):
    g = brute_force.state.g()

    with recv_event:
        if not recvlock.acquire(False):
            return recv_event.wait()

    brute_force.point("recv")
    if g.is_ready:  # don't receive anything else
        recvlock.acquire()

    recvlock.release()
    Connection_dispatch(brute_force)

    with recv_event:
        recv_event.notify_all()


def Connection_dispatch(brute_force):
    g = brute_force.state.g()

    g.is_ready = True
""".strip()
        process_code(_, threads)


if True:
    print()
    print("rpyc 5.3.1 fixed")
    for threads in range(1, MAX_THREADS + 1):
        print()
        print("  threads:", threads)

        _ = """
recv_event = None
recvlock = None

def reset(brute_force):
    global recv_event
    global recvlock

    recv_event = brute_force.Condition("recv_event")
    recvlock = brute_force.Lock("recvlock")


def AsyncResult_wait(brute_force):
    g = brute_force.state.g()

    while AsyncResult__waiting(brute_force):
        Connection_serve(brute_force, waiting=lambda: AsyncResult__waiting(brute_force))


def AsyncResult__waiting(brute_force):
    g = brute_force.state.g()

    expired = False

    return not (g.is_ready or expired)


def Connection_serve(brute_force, waiting=lambda: True):
    g = brute_force.state.g()

    with recv_event:
        if not recvlock.acquire(False):
            if not waiting():
                return False
            return recv_event.wait()

    if not waiting():
        recvlock.release()
        with recv_event:
            recv_event.notify_all()
        return False

    brute_force.point("recv")
    if g.is_ready:  # don't receive anything else
        recvlock.acquire()

    Connection_dispatch(brute_force)

    with recv_event:
        recv_event.notify_all()

    return True


def Connection_dispatch(brute_force):
    g = brute_force.state.g()

    g.is_ready = True
    recvlock.release()
""".strip()
        process_code(_, threads)
