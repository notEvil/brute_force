import brute_force
import functools


try:
    import rich.console as r_console

except ImportError:
    rich = False

else:
    rich = True
    _ = r_console.Console().print
    print = functools.partial(_, highlight=False, overflow="ignore", crop=False)


code_string = """
def f(brute_force):
    g = brute_force.state.g()

    g.a += 1
    brute_force.point("p1")

    raise Exception
""".strip()

globals = {}
exec(code_string, globals)

_ = brute_force.Thread(id="T1")
_ = {_: {globals["f"]}, brute_force.Thread(id="T2"): {globals["f"]}}
states = brute_force.process_code(brute_force.State(global_objects=dict(a=0)), _)

bad_states = filter(
    lambda state: all(len(at) == 1 and at[0].id == "p1" for at in state.ats.values())
    and state.g().a != 2,
    states,
)

for state in bad_states:
    print()
    print(brute_force.format_history(state, states, code_string=code_string, rich=rich))
